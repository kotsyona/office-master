<!-- Start tab filter-->
  <?php if (isset($message)) { ?>
    <?php echo $message; ?>
  <?php } ?>

  <?php if ($category_options) { ?>
  <style type="text/css">
    #form label{cursor:pointer;display:block;margin-left:5px;padding-left:5px;}
    #form label + label{border-top: 1px solid #ECECEC;}
  </style>
  <table class="form">
    <?php foreach ($category_options as $category_option) { ?>
    <tr>
      <td><b><?php echo $category_option['name']; ?></b></td>
      <td>
      <?php if ($category_option['type'] == 0) { ?>

        <?php if ($category_option['category_option_values']) { ?>
          <?php foreach ($category_option['category_option_values'] as $category_option_value) { ?>
            <?php if (in_array($category_option_value['value_id'], $product_to_value_id)) { ?>
            <label><input type="checkbox" name="product_to_value_id[<?php echo $category_option['option_id']; ?>][]" value="<?php echo $category_option_value['value_id']; ?>" checked="checked" />&nbsp;<?php echo $category_option_value['language'][$language_id]['name']; ?></label>
            <?php } else { ?>
            <label><input type="checkbox" name="product_to_value_id[<?php echo $category_option['option_id']; ?>][]" value="<?php echo $category_option_value['value_id']; ?>">&nbsp;<?php echo $category_option_value['language'][$language_id]['name']; ?></label>
            <?php } ?>
          <?php } ?>
        <?php } ?>
        
      <?php } else if ($category_option['type'] == 1) { ?>
          <?php if ($category_option['category_option_values']) { ?>
            <?php foreach ($category_option['category_option_values'] as $category_option_value) { ?>
              <?php if (in_array($category_option_value['value_id'], $product_to_value_id)) { ?>
              <label><input type="radio" name="product_to_value_id[<?php echo $category_option['option_id']; ?>]" value="<?php echo $category_option_value['value_id']; ?>" checked="checked" />&nbsp;<?php echo $category_option_value['language'][$language_id]['name']; ?></label>
              <?php } else { ?>
              <label><input type="radio" name="product_to_value_id[<?php echo $category_option['option_id']; ?>]" value="<?php echo $category_option_value['value_id']; ?>" />&nbsp;<?php echo $category_option_value['language'][$language_id]['name']; ?></label>
              <?php } ?>
            <?php } ?>
          <?php } ?>
      <?php } else if ($category_option['type'] == 2) { ?>
          <select name="product_to_value_id[]">
            <?php if ($category_option['category_option_values']) { ?>
              <?php foreach ($category_option['category_option_values'] as $category_option_value) { ?>
                <?php if (in_array($category_option_value['value_id'], $product_to_value_id)) { ?>
                <option value="<?php echo $category_option_value['value_id']; ?>" selected><?php echo $category_option_value['language'][$language_id]['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $category_option_value['value_id']; ?>"><?php echo $category_option_value['language'][$language_id]['name']; ?></option>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          </select>
      <?php } ?>
      </td>
    </tr>
  <?php } ?>
  </table>   
  <?php } ?>
<!-- End tab filter-->