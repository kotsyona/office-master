<form id="add" style="background-color: #FFFBE8; display:inline-block;border: 1px solid #F5EFD1;overflow:hidden;">
  <div style="float:left;width:520px;">
    <table class="form">
      <tr>
        <td>Название опции</td>
        <td>
        <?php foreach ($languages as $language) { ?>
          <input type="text" name="category_option_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo (isset($name[$language['language_id']]) ? $name[$language['language_id']]['name'] : ''); ?>" />&nbsp;<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
        <?php } ?>
        </td>
      </tr>
      <tr>
        <td>Категории</td>
        <td><div class="scrollbox">
              <?php $class = 'odd'; ?>
              <?php foreach ($categories as $category) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
              <div class="<?php echo $class; ?>">
                <?php if (in_array($category['category_id'], $option_categories)) { ?>
                <input type="checkbox" name="category_id[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                <?php echo $category['name']; ?>
                <?php } else { ?>
                <input type="checkbox" name="category_id[]" value="<?php echo $category['category_id']; ?>" />
                <?php echo $category['name']; ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
        </td>
      </tr>
      <tr>
        <td>Сортировка</td>
        <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="2" /></td>
      </tr>
      <tr>
        <td>Тип</td>
        <td>
	       <label><input type="radio" <?php echo ($type == 0) ? 'checked' : ''; ?> name="type" value="0"/> Checkbox</label>
	       <!--<label><input type="radio" <?php echo ($type == 1) ? 'checked' : ''; ?> name="type" value="1"/> Radio</label>
	       <label><input type="radio" <?php echo ($type == 2) ? 'checked' : ''; ?> name="type" value="2"/> Selectbox</label>-->
        </td>
      </tr>
    </table>
  </div>
  <div style="padding-left:540px;">
    <?php $option_value_row = 0; ?>
    <table id="block_option" style="width: 230px;">
      <thead>
        <tr><th align="left" style="width: 170px;">Значение опции</th><th align="center">Удалить</th></tr>
      </thead>
      <?php if ($option_values) { ?>
      <?php foreach ($option_values as $option_value) { ?>
        <tr id="option_value<?php echo $option_value['value_id']; ?>" class="option">
          <td style="width: 170px;background-color: #F5EFD1;padding:4px;">
            <?php foreach ($languages as $language) { ?>
              <input type="text" name="option_value[<?php echo $option_value['value_id']; ?>][language][<?php echo $language['language_id']; ?>][name]" value="<?php echo (isset($option_value['language'][$language['language_id']]) ? $option_value['language'][$language['language_id']]['name'] : ''); ?>" />&nbsp;<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
            <?php } ?>
          </td>
          <td align="center">
            <a onclick="removeValue('<?php echo $option_value['value_id']; ?>');" class="button"><span>Х</span></a>
          </td>
        </tr>
        <?php $option_value_row++; ?>
      <?php } ?>
      <?php } ?>
    </table>
    <a onclick="addValue();" class="button"><span>Добавить значение</span></a><a onclick="insertOption(<?php echo $option_id; ?>);" class="button"><span>Сохранить</span></a>
  </div>
</form>

<script type="text/javascript"><!--

var option_value_row = <?php echo $option_value_row; ?>;

function addValue() {
	html  = '<tr id="option_value' + option_value_row + '" class="option">';
	html  += '<td style="width: 170px;background-color: #F5EFD1;padding:4px;">';
	<?php foreach ($languages as $language) { ?>
    html += '<input type="text" name="option_value[' + option_value_row + '][language][<?php echo $language['language_id']; ?>][name]" value="Значение ' + option_value_row + '" />&nbsp;<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
  <?php } ?>	
  	html += '</td>';
    html += '<td align="center"><a onclick="removeValue(' + option_value_row + ');" class="button"><span>X</span></a></td>';
	html += '</tr>';

	$('#block_option').append(html);

	option_value_row++;
}

function removeValue(option_value_row) {
	$('#option_value' + option_value_row).remove();
}

function insertOption(option_id) {

  $.ajax({
		type: 'POST',
		url: 'index.php?route=module/filter/insert&token=<?php echo $token; ?>&option_id=' + option_id,
		dataType: 'html',
		data: $('#add').serialize(),
		beforeSend: function() {
		},
		complete: function() {
		},
		success: function(html) {
      $('#result').html(html);
		}
	});
   
  $('#add').remove();

}


//--></script>