<?php echo $header; ?>
<script type="text/javascript" src="view/javascript/obf.hax.fullajax.js"></script>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
 <style type="text/css">
    .list th{background-color:#D6EDD1;}
    .list{background-color: #E8FFE3;}
    #result{margin: 3px 0pt 7px; font-weight: bold;}
  </style>
<div class="box">
  <div class="left"></div>
  <div class="right"></div>
  <div class="heading">
    <h1 style="background-image: url('view/image/module.png');"><?php echo $heading_title; ?>&nbsp;&nbsp;<a href="http://vkontakte.ru/so_or" target="_blank" style="color:#DAE0E3;text-decoration:none;">By SooR</a></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
        <tr>
          <td><?php echo $entry_position; ?></td>
          <td><select name="filter_position">
              <?php foreach ($positions as $position) { ?>
              <?php if ($filter_position == $position['position']) { ?>
              <option value="<?php echo $position['position']; ?>" selected="selected"><?php echo $position['title']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $position['position']; ?>"><?php echo $position['title']; ?></option>
              <?php } ?>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td><select name="filter_status">
              <?php if ($filter_status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_sort_order; ?></td>
          <td><input type="text" name="filter_sort_order" value="<?php echo $filter_sort_order; ?>" size="1" /></td>
        </tr>
      </table>
      
      <a onclick="getForm(0);" class="button" style="margin-bottom: 8px;"><span>Добавить опцию</span></a>
      <div id="result"></div>
      <!-- list start -->
      <table class="list">
        <thead>
          <tr>
            <th width="1" align="center"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></th>
            <th align="center">Тип</th>
            <th class="left">Название</th>
            <th class="left">Значения</th>
            <th class="left">Категория</th>
            <th class="right">Сорт.</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th colspan="6" width="1" align="left"><a onclick="deleteOption();" style="margin-left: 3px;"><img src="view/image/delete.png"></a></th>
          </tr>
        </tfoot>
        <tbody>
          <?php if ($options) { ?>
          <?php foreach ($options as $option) { ?>
          <tr>
            <th align="center"><input type="checkbox" name="selected[]" value="<?php echo $option['option_id']; ?>" /></th>
            <td align="center" width="1%">
              <?php if($option['type'] == 0) { ?>
              <input type="checkbox" />
              <?php } elseif ($option['type'] == 1) { ?>
              <input type="radio" name="radio" />
              <?php } elseif ($option['type'] == 2) { ?>
              <select>
                <option>Sel</option>
              </select>
              <?php } ?>
            </td>
            <td class="left"><a href="" onclick="getForm(<?php echo $option['option_id']; ?>);return false;"><?php echo $option['language'][$language_id]['name']; ?></a></td>
            <td>
              <?php foreach ($option['option_values'] as $option_value) { ?>
                <?php echo $option_value['language'][$language_id]['name']; ?><?php echo (next($option['option_values']) ? ', ' : ''); ?> 
              <?php } ?>
            </td>
            <td class="left">
            <?php foreach ($option['categories'] as $category) { ?>
               <?php echo $category['name']; ?><?php echo (next($option['categories']) ? ', ' : ''); ?> 
            <?php } ?>
            </td>
            <td class="right"><?php echo $option['sort_order']; ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="6">Опций не обнаружено</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </form>
    <div id="getForm"></div>
  </div>
</div>

<script type="text/javascript"><!--

function getForm(option_id) {
  $('#getForm').load('index.php?route=module/filter/getform&token=<?php echo $token; ?>&option_id=' + option_id);
}

function deleteOption() {

	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/filter/delete&token=<?php echo $token; ?>',
		dataType: 'html',
		data: 'selected[]=' + encodeURIComponent($('input[name=\'selected[]\']:checked').val() ? $('input[name=\'selected[]\']:checked').val() : ''),
		beforeSend: function() {
		},
		complete: function() {
		},
		success: function() {
      $('#result').html('<b>Удалено!</b>');
		}
	});
}
//--></script>
<?php echo $footer; ?>