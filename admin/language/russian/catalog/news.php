<?php
// Heading
$_['heading_title']      		= 'Статьи, обзоры';

// Text
$_['text_success']       		= 'Изменения в статье успешно внесены!';
$_['text_thumbnail']     		= 'Миниатюра';
$_['text_fullsize']      		= 'Полный размер';
$_['text_image_manager'] 		= 'Менеджер изображений';

// Column
$_['column_title']       		= 'Заголовок';
$_['column_status']      		= 'Состояние';
$_['column_action']      		= 'Действие';

// Entry
$_['entry_title']        		= 'Заголовок:';
$_['entry_keyword']      		= 'SEO псевдоним:';
$_['entry_description']  		= 'Содержание:';
$_['entry_shot_description']  	= 'Краткое описание:<br /><span class="help">Краткое описание обязательно закрывайте символами ... (тремя точками)</span>';
$_['entry_status']		 		= 'Состояние:';
$_['entry_image']		 		= 'Изображение:';
$_['entry_image_size']	 		= 'Размер картинки:';

// Error
$_['error_permission']       	= 'У Вас нет прав для изменения статей!';
$_['error_title']        		= 'Заголовок должен быть от 3 до 64 символа!';
$_['error_description']  		= 'Содержание должно быть от 3 символов!';
$_['error_shot_description']  	= 'Краткое описание должно быть от 3 символов!';
$_['error_required_data']    = 'Не введены необходимые данные!';
?>
