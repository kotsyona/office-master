<?php
// Heading
$_['heading_title']    = 'Импорт из CSV';

// Text
$_['text_csvmenu']     = 'Импорт из CSV';
$_['text_backup']      = 'Download Backup';
$_['text_success']     = 'Success: You have successfully imported your csv file!';

// Entry
$_['entry_export']     = 'Export:';
$_['entry_import']     = 'Импорт:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify csv!';
$_['error_empty']      = 'Warning: The file you uploaded was empty!';
?>