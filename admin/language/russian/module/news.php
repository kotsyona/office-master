<?php
// Heading
$_['heading_title']    = 'Статьи, обзоры';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_left']        = 'Слева';
$_['text_right']       = 'Справа';
$_['text_homepage']    = 'Главная';

// Entry
$_['entry_limit']      = 'Выводить, статей:';
$_['entry_position']   = 'Позиция:';
$_['entry_status']     = 'Состояние:';
$_['entry_sort_order'] = 'Порядок сортировки:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_required_data'] = 'Проверьте корректность заполнения полей!';
?>
