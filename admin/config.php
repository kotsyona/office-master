<?php
// HTTP
define('HTTP_SERVER', 'http://office-master.weteam.biz/admin/');
define('HTTP_CATALOG', 'http://office-master.weteam.biz/');
define('HTTP_IMAGE', 'http://office-master.weteam.biz/image/');

// HTTPS
define('HTTPS_SERVER', 'http://office-master.weteam.biz/admin/');
define('HTTPS_IMAGE', 'http://office-master.weteam.biz/image/');

// DIR
define('DIR_APPLICATION', '/home/oficemastr/public_html/admin/');
define('DIR_SYSTEM', '/home/oficemastr/public_html/system/');
define('DIR_DATABASE', '/home/oficemastr/public_html/system/database/');
define('DIR_LANGUAGE', '/home/oficemastr/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/oficemastr/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/oficemastr/public_html/system/config/');
define('DIR_IMAGE', '/home/oficemastr/public_html/image/');
define('DIR_CACHE', '/home/oficemastr/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/oficemastr/public_html/download/');
define('DIR_LOGS', '/home/oficemastr/public_html/system/logs/');
define('DIR_CATALOG', '/home/oficemastr/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'oficemas_oficmas');
define('DB_PASSWORD', 'h3psR70O*t$B');
define('DB_DATABASE', 'oficemas_oficmas');
define('DB_PREFIX', 'oc_');
?>