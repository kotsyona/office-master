<?php
class ModelToolCSV extends Model {

	public function getTables() {
		$table_data = array();

		$query = $this->db->query("SHOW TABLES FROM `" . DB_DATABASE . "`");

		foreach ($query->rows as $result) {
			$table_data[] = $result['Tables_in_' . DB_DATABASE];
		}

		return $table_data;
	}

	public function csvExport($table) {

		$output 	= '';
	    //$query 		= "SELECT * FROM " . DB_PREFIX . $table;
		$query 		= "SELECT * FROM `" . $table . "`"; // prefix already part of the table name being passed in
	    $result 	= $this->db->query($query);
	    $columns 	= array_keys($result->row);

		$csv_terminated = "\n";
	    $csv_separator = ",";
	    $csv_enclosed = '"';
	    $csv_escaped = "\\"; //linux
		$csv_escaped = '"';

		// Header Row
	 	$output .= '"' . $table . '.' . stripslashes(implode('","' . $table . '.',$columns)) . "\"\n";

	 	// Data Rows
	    foreach ($result->rows as $row) {
			//$output .= '"' . html_entity_decode(implode('","', str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, str_replace(array("\r","\n","\t"), "", $row))), ENT_COMPAT, "utf-8") . "\"\n";

			$schema_insert = '';
			$fields_cnt = count($row);
			foreach ($row as $k => $v) {
		        if ($row[$k] == '0' || $row[$k] != '') {
		            if ($csv_enclosed == '') {
		                $schema_insert .= $row[$k];
		            } else {
		            	$row[$k] = str_replace(array("\r","\n","\t"), "", $row[$k]);
		            	$row[$k] = html_entity_decode($row[$k], ENT_COMPAT, "utf-8");
		                $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$k]) . $csv_enclosed;
		            }
		        } else {
		            $schema_insert .= '';
		        }

		        if ($k < $fields_cnt - 1) {
		            $schema_insert .= $csv_separator;
		        }
		    }

		    $output .= $schema_insert;
		    $output .= $csv_terminated;

	    }

	    return $output;
	}

	public function csvImport($file) {
		ini_set('max_execution_time', 999999);
		$groups = array(
			6 => 'Оптовики',
			12 => 'Опт1',
			13 => 'Опт2',
			14 => 'Опт3',
			15 => 'Сервис1',
			16 => 'Сервис2',
			17 => 'Сервис3',
		);
		$file = file_get_contents($file);
		$lines = explode("\n", $file);
		$count_update = $count_insert = 0;
		header('Content-type: text/html; charset=utf-8');
		echo '<pre>Начинаем импорт<br/>';

        /* WPLG */
        /* Set "import_callback" field to '0' for all product */
        $this->db->query('update ' . DB_PREFIX . 'product set import_callback=0');
        /* END WPLG */

		//var_dump($lines);
		foreach($lines as $l) {
			echo '------------------------<br/>';
			$fields = explode(';', $l);
			if (!isset($fields[1]) or !isset($fields[2]) or !isset($fields[3]) or !isset($fields[4]) or !isset($fields[5])) {
				echo 'Кривая строка -> следующий товар<br/>';
				continue;
			}
			$category_name = trim($fields[1]);
			echo 'Проверяем категорию "'.$category_name.'"<br/>';
			$q = $this->db->query('select * from '.DB_PREFIX.'category_description where lower(name) = "'.mb_strtolower($this->db->escape($category_name), 'utf-8').'"');
			if (!$q->row) {
				echo 'Категория на найдена -> следующий товар<br/>';
				continue;
			}
			
			$category_id = $q->row['category_id'];
			$code = trim($fields[2]);
			$model = htmlspecialchars(trim($fields[3]));
			$name = htmlspecialchars(trim($fields[4]));
			$price1 = str_replace(',', '.', trim($fields[6]));

            /* WPLG */
            $isInStock = (int)trim($fields[14]); /* 1 - if is in stock, else - 0 */
            /* END WPLG */

			//var_dump($price1);die;
			if (!$code or !$model or !$name or $price1 === null) {
				echo 'Недостаточно данных -> следующий товар<br/>';
				continue;
			}
			$prices = array();
			$prices[6] = str_replace(',', '.', trim($fields[7])); //Оптовики
			$prices[12] = str_replace(',', '.', trim($fields[8])); //Опт1
			$prices[13] = str_replace(',', '.', trim($fields[9])); //Опт2
			$prices[14] = str_replace(',', '.', trim($fields[10])); //Опт3
			$prices[15] = str_replace(',', '.', trim($fields[11])); //Сервис1
			$prices[16] = str_replace(',', '.', trim($fields[12])); //Сервис2
			$prices[17] = str_replace(',', '.', trim($fields[13])); //Сервис3

			echo 'Проверяем товар по коду: `'.$code.'`<br/>';
			$q = $this->db->query('select * from '.DB_PREFIX.'product where code="'.$code.'"');
			if ($q->row) {
				echo 'Товар найден -> обновляем цену, название и артикул<br/>';

                /* WPLG
				$this->db->query('update '.DB_PREFIX.'product set price="'.$price1.'", model="'.$this->db->escape($model).'" where code="'.$code.'" limit 1');
				$product_id = $q->row['product_id'];
                END WPLG */

                /* WPLG */
                if($isInStock == 1) {
                    /* If is in stock == 1, set stock_status_id=7 (Есть на складе) */
                    $this->db->query('update ' . DB_PREFIX . 'product set price="' . $price1 . '", model="' . $this->db->escape($model) . '", stock_status_id=7, status=1, import_callback=1 where code="' . $code . '" limit 1');
                    $product_id = $q->row['product_id'];
                } else {
                    /* If is in stock == 0, set stock_status_id=6 (Ожидаем поступление) */
                    $this->db->query('update ' . DB_PREFIX . 'product set price="' . $price1 . '", model="' . $this->db->escape($model) . '", stock_status_id=6, status=1, import_callback=1 where code="' . $code . '" limit 1');
                    $product_id = $q->row['product_id'];
                }
                /* END WPLG */

				$q_desc_exists = $this->db->query('select * from '.DB_PREFIX.'product_description where product_id="'.$product_id.'" limit 1');
				if($q_desc_exists->row) {
					$this->db->query('update '.DB_PREFIX.'product_description set name="'.$this->db->escape($name).'", meta_title = "' . $this->db->escape($name) . '" where product_id="'.$product_id.'" limit 1');
				} else {
					$this->db->query('insert into '.DB_PREFIX.'product_description (name,meta_title,product_id,language_id) values ("'.$this->db->escape($name).'","' . $this->db->escape($name) . '",'.$product_id.',1)');
				}

                /* WPLG
				$q_category = $this->db->query('select product_id from '.DB_PREFIX.'product_to_category where product_id="'.$product_id.'" limit 1');
				if($q_category->row) {
					$this->db->query('update '.DB_PREFIX.'product_to_category
						set category_id="'.$category_id.'"
						where product_id="'.$product_id.'" limit 1');
				} else {
					$this->db->query('insert into '.DB_PREFIX.'product_to_category
						set product_id='.$product_id.',
						category_id='.$category_id);
				}
                END WPLG */

                /* WPLG */
                $q_category = $this->db->query('select product_id from ' . DB_PREFIX . 'product_to_category where product_id="' . $product_id . '" and category_id="' . $category_id . '" limit 1');
                if (!$q_category->row) {
                    $this->db->query('insert into ' . DB_PREFIX . 'product_to_category
						set product_id=' . $product_id . ',
						category_id=' . $category_id);
                }
                /* END WPLG */

				$q_alias = $this->db->query('select * from '.DB_PREFIX.'url_alias where query=\'product_id="'.(int)$product_id.'"\' limit 1');
				$alias_slug = $this->db->escape(sprintf('%s-%s', Slug::generate($name), $product_id));
				if ($q_alias->row) {
					$this->db->query("update " . DB_PREFIX . "url_alias set keyword='" . $alias_slug . "' where query='product_id=" . (int)$product_id . "'");
				} else {
					$this->db->query("insert into " . DB_PREFIX . "url_alias
						set keyword='" . $alias_slug . "',
						query='product_id=" . (int)$product_id . "'
					");
				}

				foreach ($groups as $g_id => $g_name) {
					echo 'Проверяем есть ли цена для группы "'.$g_name.'"<br/>';
					$qq = $this->db->query('select * from '.DB_PREFIX.'product_discount where customer_group_id='.$g_id.' and product_id='.$product_id);
					if ($qq->row) {
						echo 'Цена для группы "'.$g_name.'" есть -> обновляем<br/>';
						$this->db->query('update '.DB_PREFIX.'product_discount set price="'.$prices[$g_id].'" where product_id='.$product_id.' and customer_group_id='.$g_id.' limit 1');
					} else {
						echo 'Цены для группы "'.$g_name.'" нет -> создаем<br/>';
						$this->db->query('insert into '.DB_PREFIX.'product_discount
							set product_id='.$product_id.',
							customer_group_id='.$g_id.',
							price="'.$prices[$g_id].'"');
					}
				}
				$count_update++;

            /* WPLG
			} else {
            END WPLG */

            /* WPLG */
            /* If product not exist and stock ==1, then create new product */
            } elseif($isInStock == 1) {
            /* END WPLG */

				echo 'Товар не найден -> создаем<br/>';

                /* WPLG
                $this->db->query('insert into '.DB_PREFIX.'product
                    set model = "'.$this->db->escape($model).'",
                    code = "'.$code.'",
                    price = "'.$price1.'",
                    weight_class_id=1,
                    length_class_id=1,
                    status=1,
                    sort_order=1,
                    stock_status_id=5,
                    date_added = NOW()
                ');
                $product_id = $this->db->getLastId();
                END WPLG */

                /* WPLG */
                $this->db->query('insert into ' . DB_PREFIX . 'product
                    set model = "' . $this->db->escape($model) . '",
                    code = "' . $code . '",
                    price = "' . $price1 . '",
                    weight_class_id=1,
                    length_class_id=1,
                    status=1,
                    sort_order=1,
                    stock_status_id=7,
                    import_callback=1,
                    date_added = NOW()
                ');
                $product_id = $this->db->getLastId();
                /* END WPLG */

                echo 'Создаем описание<br/>';
                $this->db->query('insert into '.DB_PREFIX.'product_description
                    set name="'.$this->db->escape($name).'",
                    meta_title="'.$this->db->escape($name).'",
                    product_id='.$product_id.',
                    language_id=1');

                if ($name) {
                    $alias_slug = $this->db->escape(sprintf('%s-%s', Slug::generate($name), $product_id));
                    $this->db->query("insert into " . DB_PREFIX . "url_alias set query = 'product_id=" . (int)$product_id . "', keyword = '" . $alias_slug . "'");
                }

                echo 'Привязываем к категории<br/>';
                $this->db->query('insert into '.DB_PREFIX.'product_to_category
                    set product_id='.$product_id.',
                    category_id='.$category_id);
                echo 'Привязываем к магазину<br/>';
                $this->db->query('insert into '.DB_PREFIX.'product_to_store
                    set product_id='.$product_id.',
                    store_id=0');
                foreach ($groups as $g_id => $g_name) {
                    echo 'Создаем цену для для группы "'.$g_name.'"<br/>';
                    $this->db->query('insert into '.DB_PREFIX.'product_discount
                        set product_id='.$product_id.',
                        customer_group_id='.$g_id.',
                        price="'.$prices[$g_id].'"');
                }
                $count_insert++;
            }
            /* WPLG */
            /* If product not exist and stock == 0, pass this product */
            else {
                echo 'Товар не существует и не будет создан поскольку имеет статус "0"<br/>';
                continue;
            }
            /* END WPLG */
        }

        /* WPLG */
        /* Set field "status" to '0' for all product WHERE "import_callback"=0 */
        $this->db->query('update ' . DB_PREFIX . 'product set status=0 where import_callback="0"');

        /* Set "mport_callback" field to '0' for all product */
        $this->db->query('update ' . DB_PREFIX . 'product set import_callback=0');
        /* END WPLG */

        echo '------------------------<br/>';
        echo '------------------------<br/>';
        echo 'Создано товаров: '.$count_insert.'<br/>';
        echo 'Обновлено товаров: '.$count_update;
        echo '</pre>';
        echo '<a href="index.php?route=tool/csv&token='.$_GET['token'].'">Вернуться в админку</a>';
        die;
        return true;
    }

    public function csvImport_old($file) {

        ini_set('max_execution_time', 999999);

        $handle = fopen($file,'r');
        if(!$handle) die('Cannot open uploaded file.');

        // Get Table name and Columns from header row
        $columns = array();
        $data = fgetcsv($handle, 1000, ",");
        foreach ($data as $d) {
            if (strpos($d, '.') !== false) {
                $tmp = explode('.', $d);
                $table = $tmp[0];
                $columns[] = $tmp[1];
            } else {
                $columns[] = $d;
            }
        }

        if (!$table) {
            exit('Could not retrieve table.');
        }

        $row_count = 0;
        $sql_query = "INSERT INTO " . DB_PREFIX . $table . "(". implode(',',$columns) .") VALUES(";

        $rows = array();

        //Read the file as csv
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $row_count++;
            $pattern = '/\A\d{1,2}\/\d{1,2}\/\d{4}/';
            $pattern2 = '/\A\d{1,2}\-\d{1,2}\-\d{4}/';
            foreach($data as $key=>$value) {
                $matches = '';
                $test = preg_match_all($pattern, $value, $matches);
                $test2 = preg_match_all($pattern2, $value, $matches);
                if ($test || $test2) {
                    //$value = str_replace('/', '-', $value);
                    $value = date("Y-m-d", strtotime($value));
                    $data[$key] = "DATE('" . $this->db->escape($value) . "')";
                } else {
                    $data[$key] = "'" . $this->db->escape($value) . "'";
                }
            }
            //$rows[] = implode(",",$data);
            $rows[] = htmlentities(implode(",",$data));
        }
        $sql_query .= implode("),(", $rows);
        $sql_query .= ")";
        fclose($handle);

        if(count($rows)) {
            $this->db->query("TRUNCATE TABLE " . DB_PREFIX . $table);
            $this->db->query($sql_query);
            $this->cache->delete('product');
        }
        return $row_count;
    }

    function validDate($date){
        //replace / with - in the date
        $date = strtr($date,'/','-');
        //explode the date into date,month and year
        $datearr = explode('-', $date);
        //count that there are 3 elements in the array
        if(count($datearr) == 3){
            list($d, $m, $y) = $datearr;
            /*checkdate - check whether the date is valid. strtotime - Parse about any English textual datetime description into a Unix timestamp. Thus, it restricts any input before 1901 and after 2038, i.e., it invalidate outrange dates like 01-01-2500. preg_match - match the pattern*/
			if(checkdate($m, $d, $y) && strtotime("$y-$m-$d") && preg_match('#\b\d{2}[/-]\d{2}[/-]\d{4}\b#', "$d-$m-$y")) { /*echo "valid date";*/
				return TRUE;
			} else {/*echo "invalid date";*/
				return FALSE;
			}
		} else {/*echo "invalid date";*/
			return FALSE;
		}
		/*echo "invalid date";*/
		return FALSE;
	}


}
?>