<?php
class ControllerModuleFilter extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/filter');

		$this->document->title = $this->language->get('heading_title');
		
		$this->load->model('setting/setting');
		$this->load->model('catalog/filter');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('filter', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect(HTTPS_SERVER . 'index.php?route=extension/module&token=' . $this->session->data['token']);
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_left'] = $this->language->get('text_left');
		$this->data['text_right'] = $this->language->get('text_right');
		
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  	$this->document->breadcrumbs = array();
   	$this->document->breadcrumbs[] = array('href'=> HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],'text' => $this->language->get('text_home'),'separator' => FALSE);
   	$this->document->breadcrumbs[] = array('href'=> HTTPS_SERVER . 'index.php?route=extension/module&token=' . $this->session->data['token'],'text'      => $this->language->get('text_module'),'separator' => ' :: ');
   	$this->document->breadcrumbs[] = array('href'=> HTTPS_SERVER . 'index.php?route=module/filter&token=' . $this->session->data['token'],'text'      => $this->language->get('heading_title'),'separator' => ' :: ');
		
		$this->data['action'] = HTTPS_SERVER . 'index.php?route=module/filter&token=' . $this->session->data['token'];
		$this->data['cancel'] = HTTPS_SERVER . 'index.php?route=extension/module&token=' . $this->session->data['token'];
		$this->data['token'] = $this->session->data['token'];

		$this->data['positions'] = array();
		$this->data['positions'][] = array('position' => 'left','title' => $this->language->get('text_left'));
		$this->data['positions'][] = array('position' => 'right','title' => $this->language->get('text_right'));
    
    if (isset($this->request->post['filter_position'])) {
			$this->data['filter_position'] = $this->request->post['filter_position'];
		} else {
			$this->data['filter_position'] = $this->config->get('filter_position');
		}
		
		if (isset($this->request->post['filter_status'])) {
			$this->data['filter_status'] = $this->request->post['filter_status'];
		} else {
			$this->data['filter_status'] = $this->config->get('filter_status');
		}
		
		if (isset($this->request->post['filter_sort_order'])) {
			$this->data['filter_sort_order'] = $this->request->post['filter_sort_order'];
		} else {
			$this->data['filter_sort_order'] = $this->config->get('filter_sort_order');
		}
		
		if (isset($this->request->post['option_name'])) {
			$this->data['option_name'] = $this->request->post['option_name'];
		} else {
			$this->data['option_name'] = $this->config->get('option_name');
		}
    
    $this->data['options'] = $this->model_catalog_filter->getOptions();

    $results = $this->model_catalog_filter->getOptions();
   
    $this->data['language_id'] = $this->config->get('config_language_id');
    	
		$this->template = 'module/filter.tpl';
		$this->children = array('common/header','common/footer');
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
  
  public function insert() {
    $output = '';
    
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('catalog/filter');
      
      if (isset($this->request->get['option_id']) && $this->request->get['option_id'] != 0) {
        $this->model_catalog_filter->updateOption($this->request->get['option_id'], $this->request->post); 
      } else {
        $this->model_catalog_filter->addOption($this->request->post); 
      } 
      $output .= 'Опции успешно сохранены. Обновите страницу.';     
    }
    
    $this->response->setOutput($output, $this->config->get('config_compression'));
  }
  
  public function delete() {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      
      $this->load->model('catalog/filter');
      
      $html = '';
      
      if (isset($this->request->post['selected'])) {
			  foreach ($this->request->post['selected'] as $option_id) {
				  $this->model_catalog_filter->deleteOption($option_id);
	  	  }
	  	  $html = 'Удалено!';
	  	}
      
		  $this->response->setOutput($html);
    }
  }
  
  public function getForm() {
    $this->load->model('catalog/filter');
    $this->load->model('localisation/language');

    if (isset($this->request->get['option_id']) && $this->request->get['option_id'] != 0 && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $option_info = $this->model_catalog_filter->getOption($this->request->get['option_id']);
    }

    if (isset($option_info)) {
      $this->data['option_id'] = $option_info['option_id'];
    } else {
      $this->data['option_id'] = 0;
    }

    if (isset($this->request->post['name'])) {
      $this->data['name'] = $this->request->post['name'];
    } elseif (isset($option_info)) {
      $this->data['name'] = $this->model_catalog_filter->getOptionDescriptions($option_info['option_id']);
    } else {
      $this->data['name'] = array();
    }
    
    if (isset($this->request->post['type'])) {
      $this->data['type'] = $this->request->post['type'];
    } elseif (isset($option_info)) {
      $this->data['type'] = $option_info['type'];
    } else {
      $this->data['type'] = '';
    }
    
    if (isset($this->request->post['option_values'])) {
      $this->data['option_values'] = $this->request->post['option_values'];
    } elseif (isset($option_info)) {
      $this->data['option_values'] = $this->model_catalog_filter->getOptionValues($this->request->get['option_id']);
    } else {
      $this->data['option_values'] = array();
    }

    if (isset($this->request->post['sort_order'])) {
      $this->data['sort_order'] = $this->request->post['sort_order'];
    } elseif (isset($option_info)) {
      $this->data['sort_order'] = $option_info['sort_order'];
    } else {
      $this->data['sort_order'] = 0;
    }
    
    $this->load->model('catalog/category');
		$this->data['categories'] = $this->model_catalog_category->getCategories(0);
		
    if (isset($this->request->post['option_categories'])) {
      $this->data['option_categories'] = $this->request->post['option_categories'];
    } elseif (isset($option_info)) {
      $this->data['option_categories'] = $this->model_catalog_filter->getOptionCategories($this->request->get['option_id']);
    } else {
      $this->data['option_categories'] = array();
    }
    
    $this->data['token'] = $this->session->data['token'];
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
    $this->data['language_id'] = $this->config->get('config_language_id');
		
		$this->template = 'module/filter_form.tpl';
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
		
  }
  
  public function category() {
		$this->load->model('catalog/product');
		
		if (isset($this->request->get['category_id'])) {
			$category_id = $this->request->get['category_id'];
		} else {
			$category_id = 0;
		}
		
		$category_data = array();
		
		$results = $this->model_catalog_product->getProductsByCategoryId($category_id);
		
		foreach ($results as $result) {
			$category_data[] = array(
				'product_id' => $result['product_id'],
				'name'       => $result['name']
			);
		}
		
		$this->load->library('json');
		
		$this->response->setOutput(Json::encode($category_data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/filter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>