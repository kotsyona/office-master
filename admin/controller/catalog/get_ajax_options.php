<?php
class ControllerCatalogGetAjaxOptions extends Controller {
	public function index() {

		$this->load->model('catalog/filter');
		$this->load->model('catalog/product');

    $this->data['category_options'] = array();

    if (isset($this->request->get['path']) && $this->request->get['path'] != '') {
			$parts = explode('_', $this->request->get['path']);
		
			foreach ($parts as $category_id) {
			  $option_info = $this->model_catalog_filter->getOptionByCategoryId($category_id);
				if ($option_info) {
          $this->data['category_options'][] = array(
            'name' => $option_info['name'],
            'type' => $option_info['type'],
            'category_option_values' => $this->model_catalog_filter->getOptionValues($option_info['option_id'])
          );
        } else {
          $this->data['message'] = 'Этой категории товаров не присвоен ниодин фильтр';
        }
			}		
		} else {
      $this->data['message'] = 'Сначала выберите категорию товаров';
    }
		
    if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		    
		$product_info = $this->model_catalog_product->getProductValues($product_id);
		
		if (isset($this->request->post['product_to_value_id'])) {
			$this->data['product_to_value_id'] = $this->request->post['product_to_value_id'];
		} elseif (isset($product_info)) {
			$this->data['product_to_value_id'] = $this->model_catalog_product->getProductValues($product_id);
		} else {
			$this->data['product_to_value_id'] = array();
		}
    
		$this->template = 'catalog/get_ajax_options.tpl';
	
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
}
?>