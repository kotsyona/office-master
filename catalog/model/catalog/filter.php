<?php

class ModelCatalogFilter extends Model {

  public function getOptionByCategoriesId($categories_id) {
  
    $data = array();
    
    foreach ($categories_id as $category_id) {
		  $data[] = "category_id = '" . (int)$category_id . "'";
		}
       
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_option co LEFT JOIN " . DB_PREFIX . "category_option_description cod ON (co.option_id = cod.option_id) WHERE co.option_id IN (SELECT option_id FROM " . DB_PREFIX . "category_option_to_category WHERE " . implode(" OR ", $data) . ") AND cod.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY sort_order");
    
    return $query->rows;
  }
  
  public function getOptionValues($option_id) {
   $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_option_value cov LEFT JOIN " . DB_PREFIX . "category_option_value_description covd ON (cov.value_id = covd.value_id) WHERE cov.option_id = '" . (int)$option_id . "' AND covd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY cov.value_id desc");
   
   return $query->rows;
  }

}

?>