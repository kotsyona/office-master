<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
      	
		$data['firstname'] = ucwords(strtolower(trim($data['firstname'])));
		$data['lastname'] = ucwords(strtolower(trim($data['lastname'])));
		$data['company'] = ucwords(strtolower(trim($data['company'])));
		$data['address_1'] = ucwords(strtolower(trim($data['address_1'])));
		$data['address_2'] = ucwords(strtolower(trim($data['address_2'])));
		$data['city'] = ucwords(strtolower(trim($data['city'])));
		$data['postcode'] = strtoupper(trim($data['postcode']));
	
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer
		SET store_id = '" . (int)$this->config->get('config_store_id') . "',
		firstname = '" . $this->db->escape($data['firstname']) . "',
		lastname = '" . $this->db->escape($data['lastname']) . "',
		email = '" . $this->db->escape($data['email']) . "',
		telephone = '" . $this->db->escape($data['telephone']) . "',
		fax = '" . $this->db->escape($data['fax']) . "',
		password = '" . $this->db->escape(md5($data['password'])) . "',
		newsletter = '" . (int)$data['newsletter'] . "',
		customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "',
		status = '1',

		manager = '".$this->db->escape($data['manager'])."',
		ur_okpo = '".$this->db->escape($data['ur_okpo'])."',
		ur_svid = '".$this->db->escape($data['ur_svid'])."',
		ur_id = '".$this->db->escape($data['ur_id'])."',
		ur_account = '".$this->db->escape($data['ur_account'])."',
		ur_bank = '".$this->db->escape($data['ur_bank'])."',
		ur_mfo = '".$this->db->escape($data['ur_mfo'])."',
		ur_number = '".$this->db->escape($data['ur_number'])."',	
		ur_code = '".$this->db->escape($data['ur_code'])."',	
		ur_address = '".$this->db->escape($data['ur_address'])."',	
		ur_company = '".$this->db->escape($data['ur_company'])."',	

		date_added = NOW()");
      	
		$customer_id = $this->db->getLastId();
			
      	$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
		
		$address_id = $this->db->getLastId();

      	$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		
		if (!$this->config->get('config_customer_approval')) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int)$customer_id . "'");
		}		
	}
	
	public function editCustomer($data) {
		$data['firstname'] = ucwords(strtolower(trim($data['firstname'])));
		$data['lastname'] = ucwords(strtolower(trim($data['lastname'])));
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET
		firstname = '" . $this->db->escape($data['firstname']) . "',
		lastname = '" . $this->db->escape($data['lastname']) . "',
		email = '" . $this->db->escape($data['email']) . "',
		telephone = '" . $this->db->escape($data['telephone']) . "',
		fax = '" . $this->db->escape($data['fax']) . "',
		newsletter = '" . (int)$data['newsletter'] . "',

		manager = '".$this->db->escape($data['manager'])."',
		ur_okpo = '".$this->db->escape($data['ur_okpo'])."',
		ur_svid = '".$this->db->escape($data['ur_svid'])."',
		ur_id = '".$this->db->escape($data['ur_id'])."',
		ur_account = '".$this->db->escape($data['ur_account'])."',
		ur_bank = '".$this->db->escape($data['ur_bank'])."',
		ur_mfo = '".$this->db->escape($data['ur_mfo'])."',
		ur_number = '".$this->db->escape($data['ur_number'])."',
		ur_code = '".$this->db->escape($data['ur_code'])."',
		ur_address = '".$this->db->escape($data['ur_address'])."',
		ur_company = '".$this->db->escape($data['ur_company'])."'

		WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function editPassword($email, $password) {
      	$this->db->query("UPDATE " . DB_PREFIX . "customer SET password = '" . $this->db->escape(md5($password)) . "' WHERE email = '" . $this->db->escape($email) . "'");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}
			
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->row;
	}
	
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) . "'");
		
		return $query->row['total'];
	}
}
?>