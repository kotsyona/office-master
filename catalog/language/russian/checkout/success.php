<?php
// Heading
$_['heading_title'] = 'Ваш заказ оформлен!';

// Text
$_['text_message']  = '<p>Ваш заказ успешно обработан!</p><p>Если у вас есть вопросы, обратитесь к <a href="index.php?route=information/contact">администратору</a>.</p><p>Спасибо за покупку!</p>';
$_['text_basket']   = 'Корзина';
$_['text_shipping'] = 'Доставка';
$_['text_payment']  = 'Оплата';
$_['text_guest']    = 'Без регистрации';
$_['text_confirm']  = 'Подтвердить';
$_['text_success']  = 'Готово';
?>