$(document).ready(function () {
    //$('.add_to_cart').removeAttr('onclick');

    $('.add_to_cart').live('click', function () {
        var pid = $(this).attr('rel');
        $.ajax({
            type: 'post',
            url: 'index.php?route=module/cart/callback',
            dataType: 'html',
            data: $('.prod-' + pid + ':first :input'),
            success: function (html) {
                $('#cart table').replaceWith(html);
                //alert('Товар добавлен в корзину.');
                $('.popok').fadeIn();
                $('.popok').animate({
                    opacity: 1

                }, 1000, function() {
                      $('.popok').fadeOut();
                });
                return false;
            }
        });
        return false;
    });
});