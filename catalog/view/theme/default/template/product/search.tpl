<?=$header?>

<div id="mid" class="inner">

<div id="midleft">

    <div class="categories">

        <?=$column_left?>

    </div>

    <!-- .Categories -->

</div>

<!-- #Midleft -->

<div id="midright">

    <div class="tc_wrap">

        <div class="tc_w_l">

            <h1><?=$heading_title?></h1>

            <div id="crumb">

                <span class="B_crumbBox">

					<? $count = 0; ?>

					<? foreach ($this->document->breadcrumbs as $b): ?>

						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>

							<span><?=$b['text']?></span>

						<? else: ?>

							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>

						<? endif; ?>

						<? $count++; ?>

					<? endforeach; ?>

                </span>

            </div><!-- #crumb-->

        </div><!-- .tc_w_l-->

        <div class="tc_w_r"></div><!-- .tc_w_r-->

    </div>

    <!-- .tc_wrap-->

    <div class="content">



        <div class="search">

            <div class="searchleft">

                <div class="searchright">

                    <form action="index.php" method="get">

                        <div class="search_wrap">

                            <div class="input-t">

                                <sub></sub>

                                <input type="hidden" name="route" value="product/search" />

								<? if ($keyword): ?>

									<input type="text" class="s_inp" name="keyword" value="<?=$keyword?>"/>

								<? else: ?>

									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>

								<? endif; ?>

								<input type="hidden" name="category_id" value="0" />

                            </div>

                            <input type="submit" value="" class="s_btn"/>

                        </div>

                    </form>

                </div><!-- .searchright-->

            </div><!-- .searchleft-->

        </div><!-- .search-->



        <!--<p class="found">Найдено <span>6</span> товаров</p>-->

		<? if (isset($products) and $products): ?>

        <div class="products">

			<? foreach ($products as $p): ?>

                <div>

					<? if ($p['leader']): ?>

                    	<div class="stat-1"></div>

					<? elseif ($p['discount']): ?>

						<div class="stat-2"></div>

					<? elseif ($p['new']): ?>

						<div class="stat-3"></div>

					<? endif; ?>

                    <div class="forimg"><a href="<?=$p['href']?>"><img src="<?=$p['thumb']?>" alt="" /></a></div>

                    <h4><?=$p['name']?></h4>

                    <?php /* WPLG */ ?>
                    <? if ($p['stock'] == 1): ?>
                    <div class="hs"><?php echo $p['stock_text']; ?></div>
                    <? else: ?>
                    <div class="nhs"><?php echo $p['stock_text']; ?></div>
                    <? endif; ?>
                    <?php /* END WPLG */ ?>

                    <dl class="prcart">

                        <dt><?=$p['price']?><? if ($p['price_old']): ?><del><?=$p['price_old']?></del><? endif; ?></dt>

                        <dd><a href="#" class="add_to_cart" rel="<?=$p['product_id']?>"><span>приобрести</span></a></dd>

                    </dl>

					<form action="" class="prod-<?=$p['product_id']?>">

						<input type="hidden" name="product_id" value="<?=$p['product_id']?>" />

						<input type="hidden" name="quantity" value="1" />

					</form>

                    <dl class="shcode">

                        <dt><?=$p['code']?></dt>

                        <dd><?=$p['model']?></dd>

                    </dl>

                    <p><?=Trim::truncate($p['description'], false, 100)?></p>

                </div>

			<? endforeach; ?>

            </div>

			<? if (isset($pagination)): ?><?php echo $pagination; ?><? endif; ?>

		<? else: ?>

			<div class="notfound">

            <p>К сожалению, поиск по вашему запросу не дал результатов.</p>

            <p>Измените критерии поиска или посмотрите разделы <a href="index.php?route=product/category/latest">новые</a> товары, <a href="index.php?route=product/category/discount">все скидки</a>, <a href="index.php?route=product/category/leaders">лидеры недели</a> — и найдете много полезных и нужных вещей!</p>

        </div>

		<? endif; ?>



    </div><!-- .content-->

    <div class="info">

        <noindex><a class="alfa" href="index.php?route=product/search/abc" rel="nofollow"><span>Поиск<br/> по алфавиту</span></a></noindex>

        <div class="load">

            <p>Скачать:</p>

            <ul>

                <li>

                    <a href="catalog.zip">Каталог</a><br/>

                    Архив ZIP

                </li>

                <li>

                    <a href="price.zip">Прайс</a><br/>

                    Архив ZIP

                </li>

            </ul>

        </div>

    </div>

<!-- .Info -->

    <div class="clr"></div>

</div>

<!-- #Midright -->

<div class="clr"></div>

</div>

<!-- #Mid -->

<?=$footer?>