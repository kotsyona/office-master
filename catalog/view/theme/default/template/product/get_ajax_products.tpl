<div class="products">

	<? foreach ($products as $p): ?>

		<div>

			<? if ($p['leader']): ?>

				<div class="stat-1"></div>

			<? elseif ($p['discount']): ?>

				<div class="stat-2"></div>

			<? elseif ($p['new']): ?>

				<div class="stat-3"></div>

			<? endif; ?>

			<div class="forimg"><a href="<?=$p['href']?>"><img src="<?=$p['thumb']?>" alt="" /></a></div>

			<h4><?=$p['name']?></h4>

            <?php /* WPLG */ ?>
            <? if ($p['stock'] == 1): ?>
            <div class="hs"><?php echo $p['stock_text']; ?></div>
            <? else: ?>
            <div class="nhs"><?php echo $p['stock_text']; ?></div>
            <? endif; ?>
            <?php /* END WPLG */ ?>

			<dl class="prcart">

				<dt><?=$p['price']?><? if ($p['price_old']): ?><del><?=$p['price_old']?></del><? endif; ?></dt>

				<dd><niondex><a href="#" class="add_to_cart" rel="nofollow"><span>приобрести</span></a></niondex></dd>

			</dl>

			<form action="" class="prod-<?=$p['product_id']?>">

				<input type="hidden" name="product_id" value="<?=$p['product_id']?>" />

				<input type="hidden" name="quantity" value="1" />

			</form>

			<dl class="shcode">

				<dt><?=$p['code']?></dt>

				<dd><?=$p['model']?></dd>

			</dl>

			<p><?=Trim::truncate($p['description'], false, 100)?></p>

		</div>

	<? endforeach; ?>

</div>

<? if (isset($pagination)): ?><?=$pagination; ?><? endif; ?>