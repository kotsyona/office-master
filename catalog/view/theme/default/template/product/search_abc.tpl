<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories" style="margin-top:160px;">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
    <div class="content">
        <div class="al-list">
            <ul class="clearfix">
				<? foreach ($abc as $key => $val) : ?>
					 <li class="<?=$val?>">
						 <a<?if ($letter == $key):?> class="active"<? endif;?> href="index.php?route=product/search/abc&letter=<?=$key?>"><?=($key)?><em></em>
						</a></li>
				<? endforeach; ?>
            </ul>
        </div>
        <div class="alphbox">
			<? if (isset($products) and $products): ?>
            <dl>
                <dd>
                    <ul>
						<? foreach ($products as $p): ?>
                        <li><a href="<?=$p['href']?>"><?=$p['name']?></a></li>
						<? endforeach; ?>
                    </ul>
                </dd>
            </dl>
			<? endif; ?>
        </div>
        <!--<div class="paginator">
            <div class="pagination clearfix">
                <span class="ditto_currentpage">1</span>
                <a href="#" class="ditto_page">2</a>
                <a href="#" class="ditto_page">3</a>
                <a href="#" class="ditto_page">4</a>
                <a href="#" class="ditto_page dt">…</a>
                <a href="#" class="ditto_page">10</a>
            </div>
        </div>-->

    </div><!-- .content-->
	<div class="info" style="margin-top:80px;">
        <div class="load" style="margin:0;">
            <p>Скачать:</p>
            <ul>
                <li>
                    <a href="catalog.zip">Каталог</a><br/>
                    Архив ZIP
                </li>
                <li>
                    <a href="price.zip">Прайс</a><br/>
                    Архив ZIP
                </li>
            </ul>
        </div>
    </div>
<!-- .Info -->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<?=$footer?>