<?php echo $header; ?>
<div id="mid" class="inner">
<div id="midleft">
    <?=$column_left?>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? elseif (!$b['href']): ?>
							<span><a class="not" href="#"><?=$b['text']?></a></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div>
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
									<input type="hidden" name="route" value="product/search" />
                                    <input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
    <div class="content wrs">
        <dl class="tpt">
            <dt><img src="<?=$popup?>" alt="" /></dt>
            <dd>
                <ul>
					<li><img rel="<?=$popup?>" src="<?=$thumb?>" /></li>
					<? if ($images): ?>
						<? foreach ($images as $i): ?>
							<li><img rel="<?=$i['popup']?>" src="<?=$i['thumb']?>" /></li>
						<? endforeach; ?>
					<? endif; ?>
                </ul>
                <dl>
                    <dt>Код: <span><?=$code?></span></dt>
                    <dd>Артикул: <span><?=$model?></span></dd>
                </dl>
                <p><?=$description?></p>
            </dd>
        </dl>
        <div class="tptr">
			<?php /* WPLG
            <? if ($stock == 1): ?>
            	<div class="hs">Есть на складе</div>
			<? else: ?>
				<div class="nhs">Нет на складе</div>
			<? endif; ?>
            END WPLG */ ?>

            <?php /* WPLG */ ?>
            <? if ($stock == 1): ?>
            <div class="hs"><?php echo $stock_text; ?></div>
            <? else: ?>
            <div class="nhs"><?php echo $stock_text; ?></div>
            <? endif; ?>
            <?php /* END WPLG */ ?>

            <? if ($discount): ?>
            <div class="action">акция</div>
            <? endif; ?>
            <form action="#" class="prod-<?=$product_id?>">
                <input type="hidden" name="product_id" value="<?=$product_id?>"/>

                <div class="tdesc">
                    <?php foreach ($options as $option) : ?>
                    <dl class="tdcol">
                        <dt><?php echo $option['name']; ?>:</dt>
                        <dd>

                            <select name="option[<?php echo $option['option_id']; ?>]">
                                <?php foreach ($option['option_value'] as $option_value) { ?>
                                <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                    <?php if ($option_value['price']) { ?>
                                    <?php echo $option_value['prefix']; ?><?php echo $option_value['price']; ?>
                                    <?php } ?>
                                </option>
                                <?php } ?>
                            </select>

                        </dd>
                    </dl>
                    <? endforeach; ?>
                    <dl class="tdlen">
                        <dt>Количество:</dt>
                        <dd>
                            <div class="inp_f_text">
                                <sub></sub><sup></sup>
                                <input type="text" value="1" name="quantity"/>
                                <ins class="plus"></ins>
                                <ins class="minus"></ins>
                            </div>
                        </dd>
                    </dl>
                    <div class="clr"></div>
                    <? if ($price_old): ?>
                    <p>Цена без скидки:
                        <del><?=$price_old?></del>
                    </p>
                    <? endif; ?>
                    <dl class="tdprice">
                        <dt><?=$price?></dt>
                        <dd>
                            <noindex><a href="#" class="add_to_cart" rel="<?=$product_id?>"><span>приобрести</span></a>
                            </noindex>
                        </dd>
                    </dl>
                </div>
            </form>

            <? if ($video): ?>
					<div class="video"><a href="<?=$video?>" rel="prettyPhoto">посмотреть<br/>видео о товаре</a></div>
				<? endif; ?>
            <div class="product_social">
                <div class="gplus">
                    <!-- Place this tag where you want the +1 button to render. -->
                    <div class="g-plusone" data-annotation="inline" data-width="335"></div>            
                </div>
                <div class="fb">
                    <div class="fb-like" data-width="335" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
                </div>
                <div class="row">
                    <!-- AddThis Button BEGIN -->
                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                        <a class="addthis_button_vk"><img src="catalog/view/theme/default/img/vkontakte_32.png" /></a>
                        <a class="addthis_button_facebook"><img src="catalog/view/theme/default/img/facebook_32.png" /></a>
                        <a class="addthis_button_twitter"><img src="catalog/view/theme/default/img/twitter_32.png" /></a>
                        <a class="addthis_button_odnoklassniki_ru"><img src="catalog/view/theme/default/img/odnoklassniki_32.png" /></a>
                        <a class="addthis_button_livejournal"><img src="catalog/view/theme/default/img/livejournal_32.png" /></a>
                        <a class="addthis_button_mymailru"><img src="catalog/view/theme/default/img/mail_ru_32.png" /></a>
                    </div>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52bc9a041c61e8aa"></script>
                    <!-- AddThis Button END -->
                </div>  
            </div>

        </div>
<div class="clr"></div>
		<? if ($products): ?>
<div class="sopt">
            <p class="st_title">Сопутствующие товары</p>
            <div class="soptlist"></div>
            <div class="clr"></div>
            <div class="tovwrap">
				<? foreach ($products as $p):?>
                <div>
                    <dl>
                        <dt><a href="<?=$p['href']?>"><img src="<?=$p['thumb']?>" alt="" /></a></dt>
                        <dd>
                            <h4><a href="<?=$p['href']?>"><?=$p['name']?></a></h4>
                            <span><?=$p['code']?></span>
                            <p><?=Trim::truncate($p['description'], false, 100)?></p>
                            <i><?=$p['price']?></i>
                        </dd>
                    </dl>
                </div>
				<? endforeach; ?>
            </div>
        </div>
<? endif; ?>

    </div><!-- .content-->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto();
  });
</script>
<?php echo $footer; ?> 