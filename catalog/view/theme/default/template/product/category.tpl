<?=$header;?>

<div id="mid" class="inner">

<div id="midleft">

    <?=$column_left?>

    <!-- .Categories -->



    <div class="info">

        <noindex><a class="alfa" href="index.php?route=product/search/abc" rel="nofollow"><span>Поиск<br/> по алфавиту</span></a></noindex>

        <div class="load">

            <p>Скачать:</p>

            <ul>

                <li>

                    <a href="catalog.zip">Каталог</a><br/>

                    Архив ZIP

                </li>

                <li>

                    <a href="price.zip">Прайс</a><br/>

                    Архив ZIP

                </li>

            </ul>

        </div>

    </div>

<!-- .Info -->



</div>

<!-- #Midleft -->

<div id="midright">

    <div class="tc_wrap">

        <div class="tc_w_l">

            <h1><?=$heading_title?></h1>

            <div id="crumb">

                <span class="B_crumbBox">

					<? $count = 0; ?>

					<? foreach ($this->document->breadcrumbs as $b): ?>

						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>

							<span><?=$b['text']?></span>

						<? elseif (!$b['href']): ?>

							<span><a class="not" href="#"><?=$b['text']?></a></span>

						<? else: ?>

							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>

						<? endif; ?>

						<? $count++; ?>

					<? endforeach; ?>

                </span>

            </div>

            <!-- #crumb-->

        </div>

        <!-- .tc_w_l-->

        <div class="tc_w_r">

            <div class="search">

                <div class="searchleft">

                    <div class="searchright">

                        <form action="index.php" method="get">

                            <div class="search_wrap">

                                <div class="input-t">

                                    <sub></sub>

									<input type="hidden" name="route" value="product/search" />

                                    <input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>

									<input type="hidden" name="category_id" value="0" />

                                </div>

                                <input type="submit" value="" class="s_btn"/>

                            </div>

                        </form>

                        <p class="ex">например: <span>цветные карандаши</span></p>

                    </div>

                    <!-- .searchright-->

                </div>

                <!-- .searchleft-->

            </div>

            <!-- .search-->

        </div>

        <!-- .tc_w_r-->

    </div>

    <!-- .tc_wrap-->

    <div class="content">

<? if (isset($sort)): ?>



        <div class="sortby">

	    <noindex>

            <ul>

                <li>Сортировать по:</li>

				<li>

					<? if ($sort == 'p.sort_order'): ?>

						<a href="<?=$sorts[0]['href']?>" class="active" rel="nofollow">популярности<em></em></a>

					<? else: ?>

						<a href="<?=$sorts[0]['href']?>" rel="nofollow">популярности<em></em></a>

					<? endif; ?>

				</li>

                <li>

					<? if ($sort . '-' . $order == 'p.price-DESC'): ?>

						<a href="<?=$sorts[3]['href']?>" class="active" rel="nofollow">цене<em></em></a>

					<? elseif ($sort . '-' . $order == 'p.price-ASC'): ?>

						<a href="<?=$sorts[4]['href']?>" class="active" rel="nofollow">цене<em></em></a>

					<? else: ?>

						<a href="<?=$sorts[3]['href']?>" rel="nofollow">цене<em></em></a>

					<? endif; ?>

				</li>

                <li>

					<? if ($sort . '-' . $order == 'pd.name-DESC'): ?>

						<a href="<?=$sorts[1]['href']?>" class="active" rel="nofollow">алфавиту<em></em></a>

					<? elseif ($sort . '-' . $order == 'pd.name-ASC'): ?>

						<a href="<?=$sorts[2]['href']?>" class="active" rel="nofollow">алфавиту<em></em></a>

					<? else: ?>

						<a href="<?=$sorts[1]['href']?>" rel="nofollow">алфавиту<em></em></a>

					<? endif; ?>

				</li>

            </ul>

	    </noindex>

        </div>

<? endif; ?>

<div id="filtit">

        <div class="products">

			<? foreach ($products as $p): ?>

                <div>

					<? if ($p['leader']): ?>

                    	<div class="stat-1"></div>

					<? elseif ($p['discount']): ?>

						<div class="stat-2"></div>

					<? elseif ($p['new']): ?>

						<div class="stat-3"></div>

					<? endif; ?>

                    <div class="forimg"><a href="<?=$p['href']?>"><img src="<?=$p['thumb']?>" alt="" /></a></div>

                    <h4><?=$p['name']?></h4>

                    <?php /* WPLG */ ?>
                    <? if ($p['stock'] == 1): ?>
                    <div class="hs"><?php echo $p['stock_text']; ?></div>
                    <? else: ?>
                    <div class="nhs"><?php echo $p['stock_text']; ?></div>
                    <? endif; ?>
                    <?php /* END WPLG */ ?>

                    <dl class="prcart">

                        <dt><?=$p['price']?><? if ($p['price_old']): ?><del><?=$p['price_old']?></del><? endif; ?></dt>

                        <dd><noindex><a href="#" class="add_to_cart" rel="<?=$p['product_id']?>"><span>приобрести</span></a></noindex></dd>

                    </dl>

					<form action="" class="prod-<?=$p['product_id']?>">

						<input type="hidden" name="product_id" value="<?=$p['product_id']?>" />

						<input type="hidden" name="quantity" value="1" />

					</form>

                    <dl class="shcode">

                        <dt><?=$p['code']?></dt>

                        <dd><?=$p['model']?></dd>

                    </dl>

                    <p><?=Trim::truncate($p['description'], false, 100)?></p>

                </div>

            <? endforeach; ?>

        </div>

		<? if (isset($pagination)): ?><?php echo $pagination; ?><? endif; ?>

</div>



    </div><!-- .content-->

	<? if (isset($category_options) and $category_options): ?>

    <noindex>

    <form action="index.php" method="get" class="filters">

		<input type="hidden" name="route" value="product/category" />

		<input type="hidden" name="path" value="<?=$path?>" />

		<input type="hidden" name="sort" value="<?=$sort?>" />

		<input type="hidden" name="order" value="<?=$order?>" />

        <div class="reset"><a href="#"><span>Сбросить фильтры</span></a></div>

		<? foreach ($category_options as $co) : ?>

			<h3><?=$co['name']?>:</h3>

				<ul>

				<? foreach ($co['category_option_values'] as $cov): ?>

					<li>

						<label>

							<input<? if (isset($value_id[$cov['option_id']]) and in_array($cov['value_id'], $value_id[$cov['option_id']])): ?> checked="checked"<? endif; ?> name="value_id[<?=$co['option_id'];?>][]" value="<?=$cov['value_id']?>" type="checkbox" />

							<?=$cov['name']?>

						</label>

					</li>

				<? endforeach; ?>

				</ul>

		<? endforeach; ?>

        

        <div class="s"><input type="submit" value="Применить" /></div>



    </form>

    </noindex>

	<script type="text/javascript">

		

		$('.reset a').click(function() {

			$('form.filters input[type=checkbox]').removeAttr('checked');

			$('form.filters').submit();

			return false;

		});

		/*

		$("form.filters").submit(function() {

			loadContent();

			return false;

		})



		function loadContent() {

		$('form.filters input[type=submit]').attr('disabled', 'disabled').val('Подождите...');

		  var $values = '';

		  var $fields = $("form.filters :input").serialize();

		  $('#filtit').load('index.php?route=product/get_ajax_products&path=<?=$filter_path?>&'+$fields, function(response, status, xhr){

			if (status == "error") {

			  error(xhr.status + " " + xhr.statusText);

			} else {

			  //effects('out', 'fade', 'load', 200);

			}

			$('form.filters input[type=submit]').removeAttr('disabled').val('Применить');

		  });

		}

		*/

	</script>

	<? endif; ?>

    <div class="clr"></div>

</div>

<!-- #Midright -->

<div class="clr"></div>

</div>

<!-- #Mid -->

<?=$footer?>