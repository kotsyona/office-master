<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
   <div class="content">
        <div class="regwrap">
            <div class="hakk">У вас уже есть аккаунт нашем магазине?<span class="enter">Войти</span></div>
            <ul>
                <li class="s1 active"><span>Физическое лицо</span></li>
                <li class="s2"><span>Юридическое лицо</span></li>
            </ul>
			<? if ($ur_okpo or $ur_svid or $ur_id or $ur_account or $ur_bank or $ur_mfo or $ur_number or $ur_code): ?>
				<script type="text/javascript">
					$(function() {
						$('li.s2').click();
					})
				</script>
			<? endif; ?>
            <form action="<?=$action?>" method="post">
                <? if ($errors): ?>
					<div class="errors">
						<? foreach ($errors as $e): ?>

							<?=$e?><br />
						<? endforeach; ?>
					</div>
				<? endif; ?>
                <h2>Ваши личные данные</h2>
                <dl>
                    <dt>Имя:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="firstname" value="<?=$firstname?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Фамилия:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="lastname" value="<?=$lastname?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Эл. почта:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="email" value="<?=$email?>"/>
                        </div>
                        <div class="notif"><label><input type="checkbox" checked="checked" name="newsletter" value="1" />Получать рассылку новостей на этот адрес</label></div>
                    </dd>
                </dl>
                <dl>
                    <dt>Телефон:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="telephone" value="<?=$telephone?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Имя вашего менеджера:</dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="manager" value="<?=$manager?>"/>
                        </div>
                    </dd>
                </dl>
				<h2>Ваш пароль</h2>
                <dl>
                    <dt>Пароль:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="password" name="password"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Повторите пароль:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="password" name="confirm"/>
                        </div>
                    </dd>
                </dl>
                <h2>Ваш адрес для доставки</h2>
                <dl>
                    <dt>Город:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
							<input type="hidden" name="country_id" value="220" />
                            <input type="text" name="city" value="<?=$city?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Адрес:<em>*</em></dt>
                    <dd>
                        <div class="tearea">
                            <sub></sub><sup></sup>
                            <textarea rows="3" cols="3" name="address_1"><?=$address_1?></textarea>
                        </div>
                    </dd>
                </dl>
                <dl class="fields">
                    <dt>&nbsp;</dt>
                    <dd><em>*</em>Поля обязательные для заполнения</dd>
                </dl>
                <!--<dl>
                    <dt>&nbsp;</dt>
                    <dd>
                        <dl class="pnr">
                            <dt><input type="checkbox" id="pnri" /></dt>
                            <dd><label for="pnri">Я хочу зарегистрировать постоянный аккаунт, чтобы повторно не вводить информацию при будущих заказах</label></dd>
                        </dl>
                    </dd>
                </dl>-->
                <dl>
                    <dt>&nbsp;</dt>
                    <dd><input type="submit" value="Продолжить&nbsp;&nbsp;&nbsp;"/></dd>
                </dl>
            </form>
            <form action="<?=$action?>" method="post">
                <? if ($errors): ?>
					<div class="errors">
						<? foreach ($errors as $e): ?>
							<?=$e?><br />
						<? endforeach; ?>
					</div>
				<? endif; ?>
                <h2>Ваши личные данные</h2>
                
                <dl>
                    <dt>Фамилия:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="lastname" value="<?=$lastname?>"/>
                        </div>
                    </dd>
                </dl>
				<dl>
                    <dt>Имя, отчество:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="firstname" value="<?=$firstname?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Эл. почта:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="email" value="<?=$email?>"/>
                        </div>
                        <div class="notif"><label><input type="checkbox" checked="checked" name="newsletter" value="1" />Получать рассылку новостей на этот адрес</label></div>
                    </dd>
                </dl>
                <dl>
                    <dt>Телефон:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="telephone" value="<?=$telephone?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Имя вашего менеджера:</dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="manager" value="<?=$manager?>"/>
                        </div>
                    </dd>
                </dl>
				<dl>
                    <dt>Компания:</dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" name="ur_company" value="<?=$ur_company?>"/>
                        </div>
                    </dd>
                </dl>
				<dl>
                    <dt>Юридический адрес:</dt>
                    <dd>
                        <div class="tearea">
                            <sub></sub><sup></sup>
                            <textarea rows="3" cols="3" name="ur_address"><?=$ur_address?></textarea>
                        </div>
                    </dd>
                </dl>
				<h2>Ваш пароль</h2>
                <dl>
                    <dt>Пароль:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="password" name="password"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Повторите пароль:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="password" name="confirm"/>
                        </div>
                    </dd>
                </dl>
                <h2>Схема налогообложения</h2>
                <div class="tax">
                    <ul class="clearfix">
                        <li class="active"><span>Единый налог</span></li>
                        <li><span>Общая форма</span></li>
                    </ul>

					<? if ($ur_okpo): ?>
						<script type="text/javascript">
							$(function() {
								$('.tax ul li:last').click();
							})
						</script>
					<? endif; ?>

						
                        <dl class="t2s">
                            <dt>Индивидуальный<br/>налоговый номер:</dt>
                            <dd>
                                <div class="inp_f_text">
                                    <sub></sub><sup></sup>
                                    <input type="text" name="ur_id" value="<?=$ur_id?>"/>
                                </div>
                            </dd>
                        </dl>
                        <dl>
                            <dt>Расчетный счет:</dt>
                            <dd>
                                <div class="inp_f_text">
                                    <sub></sub><sup></sup>
                                    <input type="text" name="ur_account" value="<?=$ur_account?>"/>
                                </div>
                            </dd>
                        </dl>
                        <dl>
                            <dt>Банк:</dt>
                            <dd>
                                <div class="inp_f_text">
                                    <sub></sub><sup></sup>
                                    <input type="text" name="ur_bank" value="<?=$ur_bank?>"/>
                                </div>
                            </dd>
                        </dl>
                        <dl>
                            <dt>МФО:</dt>
                            <dd>
                                <div class="inp_f_text">
                                    <sub></sub><sup></sup>
                                    <input type="text" name="ur_mfo" value="<?=$ur_mfo?>"/>
                                </div>
                            </dd>
                        </dl>
				<div>
				<dl>
					<dt>Код:</dt>
					<dd>
						<div class="inp_f_text">
							<sub></sub><sup></sup>
							<input type="text" name="ur_code" value="<?=$ur_code?>"/>
						</div>
					</dd>
				</dl>
				<dl class="t2s">
					<dt>Идентификационный<br/>номер плательщика<br/>единого налога:</dt>
					<dd>
						<div class="inp_f_text">
							<sub></sub><sup></sup>
							<input type="text" name="ur_number" value="<?=$ur_number?>"/>
						</div>
					</dd>
				</dl>
				
				</div>
				<div>
					<dl>
						<dt>ОКПО:</dt>
						<dd>
							<div class="inp_f_text">
								<sub></sub><sup></sup>
								<input type="text" name="ur_okpo" value="<?=$ur_okpo?>"/>
							</div>
						</dd>
					</dl>
                    <dl class="t2s">
						<dt>№ свидетельства<br/>плательщика НДС:</dt>
						<dd>
							<div class="inp_f_text">
								<sub></sub><sup></sup>
								<input type="text" name="ur_svid" value="<?=$ur_svid?>"/>
							</div>
						</dd>
					</dl>  
                </div>
				</div>
                <h2>Ваш адрес для доставки</h2>
                <dl>
                    <dt>Город:<em>*</em></dt>
                    <dd>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
							<input type="hidden" name="country_id" value="220" />
                            <input type="text" name="city" value="<?=$city?>"/>
                        </div>
                    </dd>
                </dl>
                <dl>
                    <dt>Адрес:<em>*</em></dt>
                    <dd>
                        <div class="tearea">
                            <sub></sub><sup></sup>
                            <textarea rows="3" cols="3" name="address_1"><?=$address_1?></textarea>
                        </div>
                    </dd>
                </dl>
                <dl class="fields">
                    <dt>&nbsp;</dt>
                    <dd><em>*</em>Поля обязательные для заполнения</dd>
                </dl>
                <!--<dl>
                    <dt>&nbsp;</dt>
                    <dd>
                        <dl class="pnr">
                            <dt><input type="checkbox" id="pnri" /></dt>
                            <dd><label for="pnri">Я хочу зарегистрировать постоянный аккаунт, чтобы повторно не вводить информацию при будущих заказах</label></dd>
                        </dl>
                    </dd>
                </dl>-->
                <dl>
                    <dt>&nbsp;</dt>
                    <dd><input type="submit" value="Продолжить&nbsp;&nbsp;&nbsp;"/></dd>
                </dl>
            </form>
        </div>
    </div>
	<!-- .content-->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<?=$footer?>