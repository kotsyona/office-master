<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
	<div class="content wrs">
        <div class="lc">
            <div class="mlc">
                <div class="mlc-1"><a href="index.php?route=account/edit"><sub></sub>Настройки профиля<em></em></a></div>
                <div class="mlc-2 active"><a href="index.php?route=account/history"><sub></sub>Мои заказы<em></em></a></div>
            </div>
			<? if ($orders): ?>
            <table>
                <tr>
                    <td>№</td>
                    <td>Дата</td>
                    <td>
                        <table>
                            <tr>
                                <td>Товары</td>
                                <td>цена</td>
                                <td>кол.</td>
                                <td>сумма</td>
                            </tr>
                        </table>
                    </td>
                    <td>Сумма заказа</td>
                    <td>Статус</td>
                </tr>
				<? foreach ($orders as $o): ?>
                <tr>
                    <td><?=$o['order_id']?></td>
                    <td><?=$o['date_added']?></td>
                    <td>
                        <span>Показать список товаров</span>
                        <table>
							<? foreach ($o['products'] as $p): ?>
                            <tr>
                                <td>
                                    <a href="<?=$p['href']?>"><?=$p['name']?></a>
                                    <dl>
										<dt>код: <?=$p['code']?></dt>
                                        <dt>артикул: <?=$p['model']?></dt>
                                    </dl>
                                </td>
                                <td><?=$p['price']?></td>
                                <td><?=$p['quantity']?></td>
                                <td><?=$p['total']?></td>
                            </tr>
							<? endforeach; ?>
                        </table>
                    </td>
                    <td><?=$o['total']?></td>
                    <td>
						<? if ($o['status'] == 'Ожидание'):?>
							<div class="st-1" title="в ожидании"></div>
						<? elseif ($o['status'] == 'Закрыт'):?>
							<div class="st-2" title="закрыт"></div>
						<? elseif ($o['status'] == 'Успешно завершен'):?>
							<div class="st-3" title="успешно завершен"></div>
						<? endif; ?>
					</td>
                </tr>
				<? endforeach; ?>
                <tr class="last">
                    <td colspan="3">Сумма за все время:</td>
                    <td><?=$total?></td>
                    <td>грн</td>
                </tr>
            </table>
			<? endif; ?>
        </div>
    </div>
	<!-- .content-->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<?=$footer?>