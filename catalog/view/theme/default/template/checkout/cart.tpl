<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
    <div class="content wrs">
		<? if ($products): ?>
        <form action="<?php echo str_replace('&', '&amp;', $action); ?>" method="post" enctype="multipart/form-data" id="ccc">
            <table class="cart">
                <tr>
                    <td>№</td>
                    <td>Товар</td>
                    <td><span>Количество<br/>шт.</span></td>
                    <td><span>Цена<br/>без скидки<br/>грн.</span></td>
                    <td>Скидка</td>
                    <td><span>Цена<br/>со скидкой<br/>грн.</span></td>
                    <td><span>Сумма<br/>грн.</span></td>
                    <td>&nbsp;</td>
                </tr>
				<? $count = 1; ?>
				<?php foreach ($products as $p) : ?>
                <tr>
                    <td><?=$count?>.</td>
                    <td>
                        <a href="<?=$p['href']?>"><?=$p['name']?></a>
                        <dl>
                            <dt>код: <?=$p['code']?></dt>
                            <dd>артикул: <?=$p['model']?></dd>
                        </dl>
                    </td>
                    <td>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" value="<?=$p['quantity']?>" name="quantity[<?php echo $p['key']; ?>]"/>
                            <ins class="plus"></ins>
                            <ins class="minus"></ins>
                        </div>
                    </td>
                    <td>
						<? if ($p['price_old']): ?><?=$p['price_old']?><? else: ?><?=$p['price']?><? endif; ?>
					</td>
                    <td>
						<?=$p['discount']?>%
					</td>
                    <td>
						<?=$p['price']?>
					</td>
                    <td>
						<?=$p['total']?>
					</td>
                    <td>
						<a href="#" class="cdel"></a>
						<input type="hidden" name="remove[<?php echo $p['key']; ?>]" value="0" />
					</td>
                </tr>
				<? $count++; ?>
				<? endforeach; ?>
                
            </table>
			<script type="text/javascript">
				$('#ccc').keypress(function(k) {
					if (k.charCode == 13) {
						return false;
					}
				});
				$('.cdel').click(function() {
					$(this).next('input:first').val(1);
					$('#ccc').submit();
					return false;
				})
			</script>
            <table class="itog">
               <!--<tr>
                    <td class="it-1">Общая сумма:</td>
                    <td class="it-2">13402,00</td>
                    <td class="it-3">грн.</td>
                </tr>-->
				<!--<tr>
                    <td class="it-1">
                        <dl>
                            <dt><a href="#"><span>Очистить корзину</span></a></dt>
                            <dd>Ваша скидка:</dd>
                        </dl>
                    </td>
                    <td class="it-2">300,00</td>
                    <td class="it-3">грн.</td>
                </tr>-->
                <tr class="last">
                    <td class="it-1">Итог:</td>
                    <td class="it-2"><?=$totals[0]['value']?></td>
                    <td class="it-3">грн</td>
                </tr>
            </table>
            <dl class="zp">
                <dt><a href="#"><span>Пересчитать</span></a></dt>
                <dd><input type="submit" value="Оформить заказ&nbsp;&nbsp;&nbsp;" onclick="window.location='index.php?route=checkout/guest_step_1';return false;"/></dd>
            </dl>
			<script type="text/javascript">
				$('.zp a').click(function() {
					$('#ccc').submit();
					return false;
				})
			</script>
        </form>
		<? endif; ?>
    </div><!-- .content-->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<?=$footer?>