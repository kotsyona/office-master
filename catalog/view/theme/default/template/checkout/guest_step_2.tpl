<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
    <div class="content">
        <div class="deliv">
            <div class="infodel">
                <h2>Информация о доставке:</h2>
                <ul>
                    <li><?=$guest['firstname']?> <?=$guest['lastname']?></li>
                    <li><?=$guest['city']?></li>
                    <li><?=$guest['address_1']?></li>
                </ul>
                <div><a href="index.php?route=checkout/guest_step_1">Изменить</a></div>
            </div>
            <form method="post" action="<?=$action?>">
                <div class="dosop">
                    <h2>Выберите удобный для вас способ оплаты:</h2>
					<?php foreach ($payment_methods as $payment_method) : ?>
                    <dl>
                        <dt>
							 <?php if ($payment_method['id'] == $payment || !$payment) { ?>
			  				<?php $payment = $payment_method['id']; ?>
							<input type="radio" name="payment_method" value="<?php echo $payment_method['id']; ?>" id="<?php echo $payment_method['id']; ?>" checked="checked" />
							<?php } else { ?>
							<input type="radio" name="payment_method" value="<?php echo $payment_method['id']; ?>" id="<?php echo $payment_method['id']; ?>" />
							<?php } ?>
						</dt>
                        <dd>
                            <label for="<?php echo $payment_method['id']; ?>"><?php echo $payment_method['title']; ?></label>
                            <? if ($payment_method['id'] == 'bank_transfer'): ?>
								<p>Для уточнения деталей с вами в ближайшее время свяжется наш менеджер.</p>
							<? endif; ?>
                        </dd>
                    </dl>
                    <? endforeach; ?>
                </div>
                <div class="dosop">
                    <h2>Выберите способ доставки:</h2>
					<?php foreach ($shipping_methods as $shipping_method) : ?>
					<?php foreach ($shipping_method['quote'] as $quote) : ?>
                    <dl>
                        <dt>
							<?php if ($quote['id'] == $shipping || !$shipping) { ?>
							<?php $shipping = $quote['id']; ?>
							<input type="radio" name="shipping_method" value="<?php echo $quote['id']; ?>" id="<?php echo $quote['id']; ?>" checked="checked" />
							<?php } else { ?>
							<input type="radio" name="shipping_method" value="<?php echo $quote['id']; ?>" id="<?php echo $quote['id']; ?>" />
							<?php } ?>
						</dt>
                        <dd>
                            <label for="<?php echo $quote['id']; ?>"><?php echo $quote['title']; ?></label>
                            <p><?php echo $quote['description']; ?></p>
                        </dd>
                    </dl>
                    <? endforeach; ?>
					<? endforeach; ?>
                </div>
				 <input type="hidden" name="agree" value="1" />
                <div><input type="submit" value="Далее&nbsp;&nbsp;&nbsp;"/></div>
            </form>
        </div>
    </div>
	<!-- .content-->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<?=$footer?>