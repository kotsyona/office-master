<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
    <div class="content wrs">
        <div class="cont_wrap clearfix">
            <div class="cont_l">
                <div class="cont_tel">
                    <h2>Контактные телефоны:</h2>
                    <dl>
                        <dt>офис:</dt>
                        <dd>
                            <ul>
                                <li>+38 (048) <em>730-90-01</em></li>
                                <li>+38 (048) 777-04-47</li>
                                <li>+38 (048) 716-42-49</li>
                            </ul>
                        </dd>
                    </dl>
					<dl>
                        <dt>факс:</dt>
                        <dd>
                            <ul>
                                <li>+38 (048) <em>730-91-87</em></li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt>моб.:</dt>
                        <dd>
                            <ul>
                                <li>+38 (067) <em>556-50-70</em></li>
                            </ul>
                        </dd>
                    </dl>
                    <dl class="skype">
                        <dt><em></em>skype:</dt>
                        <dd>
                            <ul>
                                <li><a href="skype:officemasterhelp">officemasterhelp</a></li>
                            </ul>
                        </dd>
                    </dl>
                </div>
                <div class="address">
                    <h2>Адрес:</h2>
                    <p>Одесса, ул. Раскидайловская, 18</p>
                    <dl>
                        <dt>e-mail:</dt>
                        <dd>
                            <ul>
                                <li><a href="mailto:contact@office-master.biz.ua">contact@office-master.biz.ua</a></li>
                            </ul>
                        </dd>
                    </dl>
                </div>
                <div class="twork">
                    <h2>Время работы:</h2>
                    <dl>
                        <dt>с 9:00 до 18:00</dt>
                        <dd>выходные — суббота, воскресенье</dd>
                    </dl>
                </div>
                <div class="callback">
                    <div><span>Заказать обратный звонок</span></div>
                    <div class="clr"></div>
                    <form action="index.php?route=information/contact/callme" method="post">
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" value="ваше имя" name="name"/>
                        </div>
                        <div class="inp_f_text">
                            <sub></sub><sup></sup>
                            <input type="text" value="ваш телефон" name="phone"/>
                        </div>
                        <div><a href="#">Заказать</a></div>
                    </form>
					<script type="text/javascript">
						$('.callback a').click(function() {
							if (!$('.callback input[name=name]').val() || $('.callback input[name=name]').val() == 'ваше имя') {
								alert('Не введено имя!');
								return false;
							}
							if (!$('.callback input[name=phone]').val() || $('.callback input[name=phone]').val() == 'ваш телефон') {
								alert('Не введен телефон!');
								return false;
							}
							$('.callback form').submit();
							return false;
						});
					</script>
                </div>
            </div>
            <!-- .cont_l-->
            <div class="cont_r">
                <form action="<?=$action?>" method="post" class="cont_r_inn">
                    <h2>Напишите нам</h2>
                    <div class="errors">
						<? if ($error_name): ?><?=$error_name?><br /><? endif; ?>
						<? if ($error_company): ?><?=$error_company?><br /><? endif; ?>
						<? if ($error_phone): ?><?=$error_phone?><br /><? endif; ?>
						<? if ($error_email): ?><?=$error_email?><br /><? endif; ?>
						<? if ($error_enquiry): ?><?=$error_enquiry?><br /><? endif; ?>
						<? if ($error_captcha): ?><?=$error_captcha?><br /><? endif; ?>
					</div>
                    <dl>
                        <dt>Имя:<em>*</em></dt>
                        <dd>
                            <div class="inp_f_text">
                                <sub></sub><sup></sup>
                                <input type="text" name="name" value="<?=$name?>"/>
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Компания:</dt>
                        <dd>
                            <div class="inp_f_text">
                                <sub></sub><sup></sup>
                                <input type="text" name="company" value="<?=$company?>"/>
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Телефон:<em>*</em></dt>
                        <dd>
                            <div class="inp_f_text">
                                <sub></sub><sup></sup>
                                <input type="text" name="phone" value="<?=$phone?>"/>
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>e-mail:<em>*</em></dt>
                        <dd>
                            <div class="inp_f_text">
                                <sub></sub><sup></sup>
                                <input type="text" name="email" value="<?=$email?>"/>
                            </div>
                        </dd>
                    </dl>
                    <dl class="tst">
                        <dt>Текст<br/>сообщения:<em>*</em></dt>
                        <dd>
                            <div class="tearea">
                                <sub></sub><sup></sup>
                                <textarea rows="3" cols="3" name="enquiry" value="<?=$enquiry?>"></textarea>
                            </div>
                        </dd>
                    </dl>
                    <dl class="tsj">
                        <dt>Контрольное<br/>число:</dt>
                        <dd>
                            <div class="inp_f_text">
                                <sub></sub><sup></sup>
                                <input type="text" name="captcha" value="<?=$captcha?>"/>
                            </div>
                            <span><img src="index.php?route=information/contact/captcha" alt=""/></span>
                        </dd>
                    </dl>
                    <dl>
                        <dt>&nbsp;</dt>
                        <dd><input type="submit" value="Отправить&nbsp;&nbsp;&nbsp;"/></dd>
                    </dl>
                </form>
            </div>
            <!-- .cont_r-->
        </div>
        <!-- .cont_wrap-->
        <div class="map">
            <h2>Карта проезда:</h2>
            <div id="map" style="width:698px; height:247px;">
				
			</div>
        </div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
		<script type="text/javascript">
			var latlng = new google.maps.LatLng(46.479944, 30.715874);
			var myOptions = {
				zoom: 17,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById('map'), myOptions);
			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
			});
		</script>
    </div>
    <!-- .content-->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<? if (isset($_GET['success'])): ?>
	<script type="text/javascript">
		alert('Спасибо! С вами обязательно свяжутся.');
	</script>
<? endif; ?>
<?=$footer?>