<?=$header?>
<div id="mid" class="inner">
<div id="midleft">
    <div class="categories">
        <?=$column_left?>
    </div>
    <!-- .Categories -->
</div>
<!-- #Midleft -->
<div id="midright">
    <div class="tc_wrap">
        <div class="tc_w_l">
            <h1><?=$heading_title?></h1>
            <div id="crumb">
                <span class="B_crumbBox">
					<? $count = 0; ?>
					<? foreach ($this->document->breadcrumbs as $b): ?>
						<? if ($count == sizeof($this->document->breadcrumbs) - 1): ?>
							<span><?=$b['text']?></span>
						<? else: ?>
							<span><a href="<?=$b['href']?>"><?=$b['text']?></a></span>
						<? endif; ?>
						<? $count++; ?>
					<? endforeach; ?>
                </span>
            </div><!-- #crumb-->
            <!-- #crumb-->
        </div>
        <!-- .tc_w_l-->
        <div class="tc_w_r">
            <div class="search">
                <div class="searchleft">
                    <div class="searchright">
                        <form action="index.php" method="get">
                            <div class="search_wrap">
                                <div class="input-t">
                                    <sub></sub>
                                    <input type="hidden" name="route" value="product/search" />
									<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
									<input type="hidden" name="category_id" value="0" />
                                </div>
                                <input type="submit" value="" class="s_btn"/>
                            </div>
                        </form>
                        <p class="ex">например: <span>цветные карандаши</span></p>
                    </div>
                    <!-- .searchright-->
                </div>
                <!-- .searchleft-->
            </div>
            <!-- .search-->
        </div>
        <!-- .tc_w_r-->
    </div>
    <!-- .tc_wrap-->
    <div class="content">

        <div class="newswrap">
			<? if (isset($news_info)): ?>
				<h2><?=$heading_title?></h2>
                <dl>
                    <dt><?=$date_added?></dt>
                    <dd><?=$description?></dd>
                </dl>
			<? else: ?>
			<? foreach ($news_data as $n): ?>
            <div>
                <a href="<?=$n['href']?>"><?=$n['title']?></a>
                <dl>
                    <dt><?=$n['date_added']?></dt>
                    <dd><?=$n['short_description']?></dd>
                </dl>
            </div>
			<? endforeach; ?>
			<? endif; ?>
            

        </div><!-- .newswrap-->
		<!--
        <div class="paginator">
            <div class="pagination clearfix">
                <span class="ditto_currentpage">1</span>
                <a href="#" class="ditto_page">2</a>
                <a href="#" class="ditto_page">3</a>
                <a href="#" class="ditto_page">4</a>
                <a href="#" class="ditto_page dt">…</a>
                <a href="#" class="ditto_page">10</a>
            </div>
        </div>-->


    </div><!-- .content-->
	<div class="info">
        <noindex><a class="alfa" href="index.php?route=product/search/abc" rel="nofollow"><span>Поиск<br/> по алфавиту</span></a></noindex>
        <div class="load">
            <p>Скачать:</p>
            <ul>
                <li>
                    <a href="catalog.zip">Каталог</a><br/>
                    Архив ZIP
                </li>
                <li>
                    <a href="price.zip">Прайс</a><br/>
                    Архив ZIP
                </li>
            </ul>
        </div>
    </div>
<!-- .Info -->
    <div class="clr"></div>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->
<?=$footer?>