<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?=$title?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="title" content=""/>
    <meta name="keywords" content="<?=$keywords?>"/>
    <meta name="description" content="<?=$description?>"/>
    <meta name="google-site-verification" content="6FUflPU-NEaFRihaEmWBZZXU6eMXyGjD7e9BAEfAhEQ" />
    <base href="<?=$base?>" />
    <link rel="stylesheet" href="catalog/view/theme/default/css/s.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="catalog/view/theme/default/css/prettyPhoto.css" type="text/css" media="screen"/>
    <!--[if lte IE 8]><link rel="stylesheet" href="catalog/view/theme/default/css/ie.css" type="text/css" media="screen"/><![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
    <!--<script src="js/jquery-1.4.3.min.js" type="text/javascript"></script>-->
    <script src="catalog/view/theme/default/js/wrapper.js" type="text/javascript"></script>
    <script src="catalog/view/theme/default/js/ease.js" type="text/javascript"></script>
    <script src="catalog/view/theme/default/js/focus.js" type="text/javascript"></script>
    <script src="catalog/view/theme/default/js/scripts.js" type="text/javascript"></script>
    <script src="catalog/view/theme/default/js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<script src="catalog/view/javascript/jquery/ajax_add.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://apis.google.com/js/platform.js">{lang: 'ru'}</script>
    <script type="text/javascript">stLight.options({publisher: "ecb0f880-8a06-49f6-a6b7-0982575b219a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/uk_UA/all.js#xfbml=1&appId=809447195738764";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper">
<div id="top">
    <div id="fastnavigation">
        <ul>
            <li><a class="fa-1" href="#">Главная</a></li>
            <li><a class="fa-2" href="/sitemap.html">Карта сайта</a></li>
            <li><a class="fa-3" href="index.php?route=information/contact">Контакты</a></li>
            <li><a class="fa-4" href="#">В избранное</a></li>
        </ul>
		<script type="text/javascript">
			$('.fa-4').click(function() {
				if (window.external) {
					window.external.AddFavorite('<?=$base?>', 'Офисмастер');
				}
				return false;
			})
		</script>
    </div>
    <!-- #Fast navigation -->
    <div id="logo">
        <a href="<?=$base?>"><img src="catalog/view/theme/default/img/logo.png" alt="Офис мастер"/></a>
    </div>
    <!-- #Logo -->
    <div id="topmenu">
        <ul class="clearfix">
            <li><a href="about_us">О компании</a></li>
            <li><a href="discounts">Скидки</a></li>
            <li><a href="payment">Оплата и доставка</a></li>
        </ul>
    </div>
    <!-- #Top menu -->
    <div id="topcontacts">
        <p class="phone"><span class="code">(048)</span> 730<em></em>90<em></em>01</p>

        <p>Одесса, ул. Раскидайловская, 18</p>
        <a class="allcontacts" href="index.php?route=information/contact">Все контакты</a>
    </div>
    <!-- #Top contacts -->
    <div id="cart">
        <noindex><a href="<?=$view?>" rel="nofollow">Корзина</a></noindex>
        <table>
            <tr>
                <td class="tdl tov">Товаров</td>
                <td class="tdr"><?=$quantity?> ед.</td>
            </tr>
            <tr>
                <td class="tdl">Итог</td>
                <td class="tdr"><?=$sum?></td>
            </tr>
        </table>
    </div>
    <!-- #Cart -->
    <div class="clr"></div>
    <div class="login">
		<? if ($this->customer->isLogged()): ?>
		Здравствуйте, <a href="index.php?route=account/edit"><?=$this->customer->getFirstName()?> <?=$this->customer->getLastName()?></a>.
		<a href="index.php?route=account/logout">Выход</a>.
		<? else: ?>
		<noindex><a href="#" class="enter" rel="nofollow">Вход</a> и <a href="index.php?route=account/create" rel="nofollow">регистрация</a></noindex>
		<? endif; ?>
	</div>
    <!-- .Login -->
    <div class="clr"></div>

</div>
<!-- #Top -->