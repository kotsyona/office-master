<?=$header;?>
<? if ($big_cats): ?>
<div id="script">
<div id="carousel">
<div class="titlewrap"></div>
<div id="wrapcar">
<div id="carwrap">
<ul class="ccontent">
<? $count = 0; ?>
<? foreach ($big_cats as $bc): ?>
<li<? if ($count == 3): ?> class="active"<? endif; ?>>
    <img src="<?=$bc['image']?>" alt="b"/>
    <div class="title">
        <div class="service closed">
            <span><em style="white-space:nowrap;"><?=$bc['name']?></em></span>
			<? if ($bc['subcategories']):?>
            <div class="ul">
                <ul>
					<? foreach ($bc['subcategories'] as $s):?>
                    <li><a href="<?=$s['href']?>"><span><?=$s['name']?></span></a></li>
					<? endforeach ?>
                </ul>
                <div class="ultop"></div>
                <div class="ulbot"></div>
            </div>
			<? endif; ?>
            <div class="fix"></div>
        </div>
    </div>
	<? if (isset($bc['product'])): ?>
    <div class="item-right">
        <div class="item-desc">
            <div class="item-name">
                <a href="<?=$bc['product']['href']?>"><?=$bc['product']['name']?></a>
                <div class="item-buy">
    
                    <span class="price"><?=$bc['product']['price']?></span>
                </div>
            </div>
            <div class="clr"></div>
            <span class="articul"><?=$bc['product']['code']?></span>
        </div>
        <div class="duga"></div>
    </div>
	<? endif; ?>
</li>
<? $count++;?>
<? endforeach; ?>
</ul>
</div>
<span class="prev preva">&laquo;</span>
<span class="next nexta">&raquo;</span>
</div>
<div class="shad"></div>
</div>
</div>
<? endif; ?>
<div id="mid">
<div id="midleft">
    <?=$column_left?>
    <!-- .Categories -->
    <div class="search">
        <div class="searchleft">
            <div class="searchright">
                <form action="index.php" method="get">
					<div class="search_wrap">
						<div class="input-t">
							<sub></sub>
							<input type="hidden" name="route" value="product/search" />
							<input type="text" class="s_inp" name="keyword" value="поиск по каталогу"/>
							<input type="hidden" name="category_id" value="0" />
						</div>
						<input type="submit" value="" class="s_btn"/>
					</div>
				</form>
                <p class="ex">например: <span>цветные карандаши</span></p>
            </div>
        </div>
    </div>
    <!-- .Search -->

</div>
<!-- #Midleft -->
<div id="midright">
<? if ($latest): ?>
<div class="newproduct">
    <p class="h2"><a href="index.php?route=product/category/latest">Новые товары</a></p>

    <div class="item">
        <div class="item-img">
            <a href="<?=$latest['href']?>"><img src="<?=$latest['image']?>" alt="item"/></a>
        </div>

        <div class="item-right">
            <div class="item-desc">
                <div class="item-cat">
                    <a href="<?=$latest['category_href']?>"><?=$latest['category_name']?></a>
                </div>
                <div class="item-name">
                    <a href="<?=$latest['href']?>"><?=$latest['name']?></a>
                </div>
                <span class="articul"><?=$latest['code']?></span>

                <div class="item-txt">
                    <?=Trim::truncate($latest['description'], false)?>
                </div>
		<noindex>

            <?php /* WPLG */ ?>
            <?php /* To display, uncomment
            <? if ($latest['stock'] == 1): ?>
            <div class="hs"><?php echo $latest['stock_text']; ?></div>
            <? else: ?>
            <div class="nhs"><?php echo $latest['stock_text']; ?></div>
            <? endif; ?>
            */ ?>
            <?php /* END WPLG */ ?>

                <div class="item-buy">
					<? if ($latest['price_old']): ?><div class="olp"><?=$latest['price_old']?></div><? endif; ?>
                    <span class="price"><?=$latest['price']?></span>
					<span class="addto add_to_cart" rel="<?=$latest['product_id']?>"><em>приобрести</em></span>
                </div>
				<form action="" class="prod-<?=$latest['product_id']?>">
					<input type="hidden" name="product_id" value="<?=$latest['product_id']?>" />
					<input type="hidden" name="quantity" value="1" />
				</form>

		</noindex>
            </div>
        </div>
        <div class="clr"></div>
    </div>

</div>
<!-- .Mew product -->
<? endif; ?>
<? if ($leaders): ?>
<div class="ln">
    <p class="h4"><a href="index.php?route=product/category/leaders">Лидеры недели</a></p>

    <div class="lider">
		<? foreach ($leaders as $l): ?>
        <div class="item">
            <div class="item-img">
                <a href="<?=$l['href']?>"><img src="<?=$l['image']?>" alt="item"/></a>
            </div>

            <div class="item-right">
                <div class="item-desc">
                    <div class="item-name">
                        <a href="<?=$l['href']?>"><?=$l['name']?></a>

                        <?php /* WPLG */ ?>
                        <?php /* To display, uncomment
                        <? if ($l['stock'] == 1): ?>
                        <div class="hs"><?php echo $l['stock_text']; ?></div>
                        <? else: ?>
                        <div class="nhs"><?php echo $l['stock_text']; ?></div>
                        <? endif; ?>
                        */ ?>
                        <?php /* END WPLG */ ?>

                        <?php /* WPLG */ ?>
                        <? if ($l['stock'] == 1): ?>
                        <?php /* END WPLG */ ?>

                        <div class="item-buy">
                        	<? if ($l['price_old']): ?><div class="olp"><?=$l['price_old']?></div><? endif; ?>
                            <span class="price"><?=$l['price']?></span>
                        </div>

                        <?php /* WPLG */ ?>
                        <? endif; ?>
                        <?php /* END WPLG */ ?>

                    </div>
                    <span class="articul"><?=$l['code']?></span>

                    <div class="item-txt">
                        <p><?=Trim::truncate($l['description'], false, 100)?></p>
                    </div>

                    <span class="addto add_to_cart" rel="<?=$l['product_id']?>"><em>Приобрести</em></span>
					<form action="" class="prod-<?=$l['product_id']?>">
						<input type="hidden" name="product_id" value="<?=$l['product_id']?>" />
						<input type="hidden" name="quantity" value="1" />
					</form>

                </div>
            </div>
            <div class="clr"></div>
        </div>
        <? endforeach; ?>

    </div>


    <div class="cntrl">

    </div>
    <div class="lntop"></div>
    <div class="lnbot"><sup></sup></div>
</div>
<!-- .Lider nedeli -->
<? endif; ?>
<div class="info">
    <noindex><a class="alfa" href="index.php?route=product/search/abc" rel="nofollow"><span>Поиск<br/> по алфавиту</span></a></noindex>

    <div class="load">
        <p>Скачать:</p>
	<noindex>
        <ul>
            <li>
                <a href="catalog.zip" rel="nofollow">Каталог</a><br/>
                Архив ZIP

            </li>
            <li>
                <a href="price.zip" rel="nofollow">Прайс</a><br/>
                Архив ZIP
            </li>
        </ul>
	</noindex>
    </div>

</div>
<!-- .Info -->
<div class="clr"></div>
<? if ($discounts): ?>
<div class="skidki">
	<? foreach ($discounts as $d): ?>
    <div class="item">
        <div class="item-img">
            <a href="<?=$d['href']?>"><img src="<?=$d['image']?>" alt="item"/></a>
        </div>

        <div class="item-right">
            <div class="item-desc">

                <div class="item-name">
                    <a href="<?=$d['href']?>"><?=Trim::truncate($d['name'], false, 70)?></a>
                </div>
                <span class="articul"><?=$d['code']?></span>

                <?php /* WPLG */ ?>
                <?php /* To display, uncomment
                <? if ($d['stock'] == 1): ?>
                <div class="hs"><?php echo $d['stock_text']; ?></div>
                <? else: ?>
                <div class="nhs"><?php echo $d['stock_text']; ?></div>
                <? endif; ?>
                */ ?>
                <?php /* END WPLG */ ?>

                <div class="item-txt">
                    <p><?=Trim::truncate($d['description'], false, 100)?></p>
                </div>

                <noindex>
                    <div class="item-buy">
                        <span class="price"><?=$d['price']?></span>
                        <span class="addto add_to_cart" rel="<?=$d['product_id']?>"><em>приобрести</em></span>
                    </div>

                    <form action="" class="prod-<?=$d['product_id']?>">
                        <input type="hidden" name="product_id" value="<?=$d['product_id']?>"/>
                        <input type="hidden" name="quantity" value="1"/>
                    </form>
                </noindex>

            </div>
        </div>
        <div class="clr"></div>
    </div>
	<?endforeach; ?> 
    <a href="index.php?route=product/category/discount" class="allskid">Все скидки</a>
</div>
<!-- .Skidki -->
<? endif; ?>
</div>
<!-- #Midright -->
<div class="clr"></div>
</div>
<!-- #Mid -->


<div id="midbot">
    <div id="main_social">
        <div class="gplus">
            <!-- Place this tag where you want the +1 button to render. -->
            <div class="g-plusone" data-annotation="inline" data-width="335" data-href="http://office-master.biz.ua/"></div>            
        </div>
        <div class="fb">
            <div class="fb-like-box" data-href="https://www.facebook.com/kanctovaryodessa" data-width="335" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
        </div>
        <div class="vk">
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>
            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "335", height: "290"}, 50938816);
            </script>
        </div>
        <div class="row">
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="http://office-master.biz.ua/" addthis:title="Интернет-магазин Одесса Офис-Мастер Канцтовары">
                <a class="addthis_button_vk"><img src="catalog/view/theme/default/img/vkontakte_32.png" /></a>
                <a class="addthis_button_facebook"><img src="catalog/view/theme/default/img/facebook_32.png" /></a>
                <a class="addthis_button_twitter"><img src="catalog/view/theme/default/img/twitter_32.png" /></a>
                <a class="addthis_button_odnoklassniki_ru"><img src="catalog/view/theme/default/img/odnoklassniki_32.png" /></a>
                <a class="addthis_button_livejournal"><img src="catalog/view/theme/default/img/livejournal_32.png" /></a>
                <a class="addthis_button_mymailru"><img src="catalog/view/theme/default/img/mail_ru_32.png" /></a>
            </div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52bc9a041c61e8aa"></script>
            <!-- AddThis Button END -->
        </div>  
    </div>
    <!-- .Social -->

    <div id="social_links">
        Мы <a href="http://vk.com/kanc_tovary_od" target="_blank"><img src="catalog/view/theme/default/img/vk-icon.png" /></a> <a href="https://www.facebook.com/kanctovaryodessa" target="_blank"><img src="catalog/view/theme/default/img/fb-icon.png" /></a> <a href="http://office-master.livejournal.com/" target="_blank"><img src="catalog/view/theme/default/img/lj-icon.png" /></a> <a href="https://plus.google.com/116939245423598611335" rel="publisher" target="_blank"><img src="catalog/view/theme/default/img/gp-icon.png" /></a> <a href="http://www.youtube.com/user/kanctovary" target="_blank"><img src="catalog/view/theme/default/img/ut_icon.png" /></a>
    </div>
    <!-- .Social -->

    <div class="news">
        <p class="h3">Новости</p>
		<? if ($news): ?>
		<? foreach ($news as $n): ?>
        <div class="nw">
            <a href="<?=$n['href']?>"><?=$n['title']?></a>

            <div class="anons">
                <span class="date"><?=$n['date']?></span>

                <?=$n['short_description']?>
            </div>
        </div>
		<? endforeach; ?>
        <a href="<?=$news_all?>">Читать остальные новости</a>
		<? else: ?>
			<p>Нет новостей</p>
		<? endif; ?>
    </div>

    <div class="subscribe closed">
        <p class="pod "><span>Рассылка новостей</span></p>

        <div class="ras">
            <form action="index.php?route=common/home/subscribe" method="post">
                <div>
                    <input type="text" class="input-r" name="email" value="ваш e-mail..."/>
                    <span id="submit-r">Подписаться</span>
                </div>
				<script type="text/javascript">
					$('#submit-r').click(function() {
						if (!$('.ras input').val() || $('.ras input').val() == 'ваш e-mail...') {
							alert('Не введен email!');
							return false;
						}
						if (!$('.ras input').val().match(/.+@.+\..+/)) {
							alert('Некорректный email!');
							return false;
						}
						$('.ras form').submit();
						return false;
					});
				</script>
            </form>
        </div>
    </div>
    <!-- .Subscribe -->

</div>
<!-- #Midbot -->


</div>
<!-- #Wrapper -->
<?php echo $footer; ?> 