<div class="categories"><?php echo $category; ?></div>
<script type="text/javascript">
	$(function() {
		$('.categories a.active').parents('li').addClass('opened').find('ul').show();
		$('.opened .subcategory ul').hide();
		$('.opened .subcategory .active').parent().find('ul').show();
		$('.subcategory .active').parent().parent().show();
	})
</script>