<?php if ($category_options) { ?>

<div class="box">
  <style type="text/css">
    #filter-item label{margin-left:5px;padding-left:5px;display: inline-block; width: 150px;cursor:pointer;}
    #filter-item select{margin-left:10px;min-width:100px;}
    #filter-item label + label{border-top: 1px solid #ECECEC;}
    #filter-item span{font-weight:bold}
    .top #load{position: absolute;left:4px;top:5px;display:none;}
  </style>
  <div class="top">
    <img src="catalog/view/theme/default/image/filter_icon.png" alt="" id="icon" />
    <img src="catalog/view/theme/default/image/filter_load.png" alt="" id="load" />
    <?php echo $heading_title; ?>
  </div>
  <div class="middle">
    <form id="filters" style="line-height: 20px;">
      <?php foreach ($category_options as $category_option) { ?>
        <div id="filter-item">
        <span><?php echo $category_option['name']; ?></span><br />
        <?php if ($category_option['category_option_values']) { ?>
          <?php if ($category_option['type'] == 0) { ?>
            <?php foreach ($category_option['category_option_values'] as $category_option_value) { ?>
            <label><input type="checkbox" name="value_id[<?php echo $category_option['option_id']; ?>]" value="<?php echo $category_option_value['value_id']; ?>">&nbsp;<?php echo $category_option_value['name']; ?></label>
            <?php } ?>
          <?php } else if ($category_option['type'] == 1) { ?>
            <label><input type="radio" name="value_id[<?php echo $category_option['option_id']; ?>]" value="">&nbsp;Все</label>
            <?php foreach ($category_option['category_option_values'] as $category_option_value) { ?>
            <label><input type="radio" name="value_id[<?php echo $category_option['option_id']; ?>]" value="<?php echo $category_option_value['value_id']; ?>">&nbsp;<?php echo $category_option_value['name']; ?></label>
            <?php } ?>
          <?php } else if ($category_option['type'] == 2) { ?>
            <select name="value_id[<?php echo $category_option['option_id']; ?>]">
              <option value="">Все</option>
              <?php foreach ($category_option['category_option_values'] as $category_option_value) { ?>
                <option value="<?php echo $category_option_value['value_id']; ?>"><?php echo $category_option_value['name']; ?></option>
              <?php } ?>
            </select>
          <?php } ?>
        <?php } ?>
        </div>
      <?php } ?>
    </form>
  </div>
  <div class="bottom">&nbsp;</div>
</div>

<script type="text/javascript"><!--

$('#filters input, #filters select').live('change', function() {
  effects('', 'jump', 'icon', 200);
  //effects('in', 'fade', 'load', 300);
	loadContent();
});
 
// Обработчик инпутов в фильтре и вытяжка результата
function loadContent() {
  var $values = '';
  var $fields = $("#filters input, #filters select").serializeArray();
  jQuery.each($fields, function(i, $field){
     if (i == 0) {
      $values += $field.value;
     } else if ($field.value == '') { 
      $values += '';
     } else {
      $values += '_' + $field.value;
     } 
  });

  $('#content').load('index.php?route=product/get_ajax_products&path=<?php echo $path; ?>&values=' + $values, function(response, status, xhr){
    if (status == "error") {
      error(xhr.status + " " + xhr.statusText);
    } else {
      //effects('out', 'fade', 'load', 200);
    }
  }); 
}
// Эффекты для прелоадера.
function effects(way, type, el, dur) {
  el = $('#' + el);
  el.parent().css({'position' : 'relative'});
  
  if (type == 'fade') {
    if (way == 'in') {
      el.fadeIn(dur);
    } else {
      el.fadeOut(dur);
    }
  }
  
  if (type == 'jump') {
    el.css({'position' : 'relative'});
    el.animate({top: '-=2px'}, dur).animate({top: '+=5px'}, dur).animate({top: '-=5px'}, dur).animate({top: '+=2px'}, dur);
  }
}
// Если jQuery чето не загрузил
function error(mess) {
  var msg = "Ошибка загрузки: ";
  $("#filters").prepend(msg + mess);
}

//--></script>

<?php } ?>
