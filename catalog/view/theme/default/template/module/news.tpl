<?php if (isset($homepage)) { ?>
  <div class="newspage">
    <div class="heading"><?php echo $heading_title; ?></div>
    <?php foreach ($news as $news_story) { ?>
      <div style="border-bottom: 1px solid #DDDDDD; margin-bottom: 8px;">
        <h4><?php echo $news_story['title']; ?></h4>
        <?php echo $news_story['short_description']; ?><a href="<?php echo $news_story['href']; ?>"><?php echo $text_read_more; ?></a></p>
      </div>
    <?php } ?>
  </div>
<?php } else { ?>
  <div class="box">
    <div class="top"><img src="catalog/view/theme/default/image/news.png" alt="" /><a href="<?php echo $news_all; ?>"><?php echo $heading_title; ?></a></div>
    <div id="articles" class="middle">
      <ul>
    <?php foreach ($news as $news_story) { ?>
        <li><a href="<?php echo $news_story['href']; ?>"><?php echo $news_story['title']; ?></a></li>
    <?php } ?>
      </ul>
    </div>
    <div class="bottom">&nbsp;</div>
  </div>
<?php } ?>
