$(document).ready(function() {

	$('a.not').click(function() {
		return false;
	});

    $('#menu li:last').addClass('lilast');

	$('.tptr form').submit(function() {
		return false;
	});
	
    //Category menu
    $('.parent').click(function() {
        if ($(this).parent().hasClass('opened')) {
            $('.opened .subcategory').slideUp(200, function() {
                $(this).parent().removeClass('opened').addClass('closed');
            });

        }
        else {
            $('.opened .subcategory').slideUp(200, function() {
                $(this).parent().removeClass('opened').addClass('closed');
            });
            $(this).next().slideDown(200, function() {
                $(this).parent().removeClass('closed').addClass('opened');
            });
        }
        return false;
    });

    /* Show/Hide Label Text on Focus & Blur */
    $('.search_wrap .s_inp').focus(function() {
        if ($(this).val() == 'поиск по каталогу') {
            this_val = $(this).val();
            $(this).attr('rel', this_val);
            $(this).val('');
        }
        else {
            return false;
        }
    });

    $('.search_wrap .s_inp').blur(function() {

        if ($(this).val() == '') {
            old_val = $(this).attr('rel');
            $(this).val(old_val);
        }
        else {
            return false;
        }
    });

    var sv = $('.search p span').text();

    $('.search p span').click(function() {
        $('.search_wrap .s_inp').attr('value', sv);

    });


    //Lider tabs
    $('.ln .item:odd').addClass('odd');

    $(function() {
        $('.lider').wrapChildren({
            childElem : '.item' ,
            sets: 2,
            wrapper: 'div class="twoset"'
        });
    });

    for (var i = 0; i < $('.twoset').length; i++) {
        $('.twoset').eq(i).attr('id', 't-' + i + '');
      //  $('.cntrl').append('<span id="p-' + i + '"></span>');

    }

    $('.twoset:first').addClass('active');
    $('.cntrl span:first').addClass('active');

   // $('.cntrl span').live('click', function() {
     //   $(this).addClass('active').siblings().removeClass('active');
     //   var id = $(this).attr('id').replace('p', 't');

     //   $('#' + id + '').addClass('active').siblings().removeClass('active');


  //  });


    // Skidki
    for (var j = 0; j < $('.skidki .item').length; j++) {
        $('.skidki .item').eq(j).attr('id', 'it-' + j + '');


    }

    $('.skidki .item:first').addClass('active').siblings('').addClass('passive');

    $('.skidki .passive .item-name a').live('click', function() {
        var current_active = $('.skidki .active').html();
        var current_passive = $(this).closest('.passive').html();
        $('.skidki .active').html(current_passive);
        $(this).closest('.passive').html(current_active);

        return false;
    });


    //Carousel
    if ($('#carwrap').length) {
        $('.ccontent .active .item-right').show();

        $('.ccontent .active').next().addClass('nextitem');
        $('.ccontent .active').prev().addClass('previtem');

        var title = $('.ccontent>.active .title').html();
        $('.titlewrap').html(title);

        var nextlabel = $('.ccontent>.active').next().find('.title').html();
        $('.next').html(nextlabel);

        var prevlabel = $('.ccontent>.active').prev().find('.title').html();
        $('.prev').html(prevlabel);


        var cwidth = $('#carwrap').width();

        function carwidth() {
            var items = $('.ccontent>li').length;
            var cwidth = $('#carwrap').width();
            $('.ccontent').css('width', '' + items * cwidth * 0.6 + 'px');
            $('.ccontent>li').css('width', '' + cwidth * 0.6 + 'px');
            $('.ccontent').css('left', '' + -2 * cwidth * 0.6 - cwidth * 0.4 + 'px');
        }

        carwidth();

        $(window).resize(function() {
            carwidth();
        });

        $('.nexta').live('click', function() {

            $('.next').removeClass('nexta');

            $('.ccontent .active .item-right').fadeOut();

            $('.ccontent>.active').removeClass('active').next().addClass('active');
            $('.ccontent .active').next().addClass('nextitem').siblings().removeClass('nextitem');
            $('.ccontent .active').prev().addClass('previtem').siblings().removeClass('previtem');


            var title = $('.ccontent>.active .title').html();
            $('.titlewrap').html(title);

            var nextlabel = $('.ccontent>.active').next().find('.title').html();
            $('.next').html(nextlabel);

            var prevlabel = $('.ccontent>.active').prev().find('.title').html();
            $('.prev').html(prevlabel);

            var firsthtml = $('.ccontent>li:first').html();
            var firstclass = $('.ccontent>li:first').attr('class');
            var mleft = $('#carwrap').width() * 0.6;

            $('.ccontent').append($('<li class=' + firstclass + '>' + firsthtml + '</li>'));


            $('.ccontent>li').css('width', '' + mleft + 'px');


            $('.ccontent').animate({

                left: '-=' + mleft + ''

            }, 700,'easeOutQuad', function() {
                $('.ccontent .active .item-right').fadeIn();
                $('.ccontent>li:first').remove();
                var cpos = parseInt($('.ccontent').css('left'));
                var newpos = parseInt(cpos + mleft);


                $('.ccontent').css('left', '' + newpos + 'px');

                $('.next').addClass('nexta');

            });


        });

        $('.preva').live('click', function() {
            $('.prev').removeClass('preva');
            $('.ccontent .active .item-right').fadeOut();

            $('.ccontent>.active').removeClass('active').prev().addClass('active');
            $('.ccontent .active').next().addClass('nextitem').siblings().removeClass('nextitem');
            $('.ccontent .active').prev().addClass('previtem').siblings().removeClass('previtem');

            var title = $('.ccontent>.active .title').html();
            $('.titlewrap').html(title);

            var nextlabel = $('.ccontent>.active').next().find('.title').html();
            $('.next').html(nextlabel);

            var prevlabel = $('.ccontent>.active').prev().find('.title').html();
            $('.prev').html(prevlabel);

            var mleft = $('#carwrap').width() * 0.6;

            var prevh = $('.ccontent>li:last').html();
            $('.ccontent>li:last').remove();

            $('.ccontent').prepend($('<li class="mmm">' + prevh + '</li>'));

            $('.ccontent>li').css('width', '' + mleft + 'px');


            $('.ccontent').animate({

                left: '+=' + mleft + ''

            }, 700,'easeOutQuad', function() {
                $('.ccontent .active .item-right').fadeIn();
                var cpos = parseInt($('.ccontent').css('left'));

                var newpos = parseInt(cpos - mleft);


                $('.mmm').removeClass('mmm');
                $('.ccontent').css('left', '' + newpos + 'px');
                $('.prev').addClass('preva');
            });


        });
    }

    $('.nextitem img').live('click',function(){
       $('.nexta').click(); 
    });

    $('.previtem img').live('click',function(){
          $('.preva').click(); 
    });

    // Select service
    $('.titlewrap .service span').live('click', function() {

        if ($(this).parent().hasClass('closed')) {
            $(this).parent().removeClass('closed').addClass('shown').find('.ul').show();
            $('.fix').show();
        }
        else {
            $(this).parent().removeClass('shown').addClass('closed').find('.ul').hide();
            $('.fix').hide();
        }
    });

    $('.fix').live('click', function() {
        $('.fix').hide();
        $('.service').removeClass('shown').addClass('closed').find('.ul').hide();
    });

    $('.robot span').click(function() {
        if ($('.rt').hasClass('opened')) {
            $('.rt').slideUp('fast', function() {
                $('.rt').removeClass('opened').addClass('closed');
            });
        }
        if ($('.rt').hasClass('closed')) {
            $('.rt').slideDown('fast', function() {
                $('.rt').removeClass('closed').addClass('opened');
            });
        }
    });

    $('.pod span').click(function() {
        if ($('.subscribe').hasClass('opened')) {
            $('.ras').slideUp(100, function() {
                $('.subscribe').removeClass('opened').addClass('closed');
            });
        }
        if ($('.subscribe').hasClass('closed')) {
            $('.ras').slideDown(100, function() {
                $('.subscribe').removeClass('closed').addClass('opened');
            });
        }
    });


    // Specific Css resolution
    function resolution() {
        if ($('body').width() <= 1100) {
            $('.ln').css('marginLeft', '1%');
            $('#topmenu').css('marginLeft', '7%');
        }
        if ($('body').width() > 1100 && $('body').width() <= 1200) {
            $('.ln').css('marginLeft', '2%');
            $('#topmenu').css('marginLeft', '11%');
        }

        if ($('body').width() > 1200) {
            $('.ln').css('marginLeft', '6%');
            $('#topmenu').css('marginLeft', '11%');
        }

    }

    resolution();

    $(window).resize(function() {
        resolution();

    });

    /*if ($('textarea').length) {


        $('textarea').clearOnFocus();

    }*/
    if ($('input[name=keyword]').length) {


        $('input[name=keyword]').clearOnFocus();
    }
	if ($('.ras input').length) {


        $('.ras input').clearOnFocus();
    }
	if ($('.callback input').length) {


        $('.callback input').clearOnFocus();
    }
    $('.ln .item-img').each(function() {
        $(this).append('<sub></sub>');
    });

    $('.callback div:first').click(function(){
        $(this).toggleClass('active');
        $('.callback form').slideToggle('fast');
    });

            /*// validator
    $('.cont_r_inn, .regwrap form').validate({
        rules : {
            username : {required : true, minlength: 2},
            lastname : {required : true, minlength: 2},
            phone : {required : true, digits: true, minlength: 3},
            email : {required : true, email: true},
            textmess : {required : true},
            password : {required : true, equalTo: true},
            password : {equalTo: true}
        },
        messages : {
            username : {required : "Введите ваше имя", minlength : "Введите ваше имя"},
            lastname : {required : "Введите вашу фамилию", minlength : "Введите вашу фамилию"},
            phone : {required : "Введите ваш телефон", digits: "Введен неверный номер телефона", minlength: "Введите ваш телефон"},
            email : {required : "Введите ваш e-mail", email : "Введен неверный e-mail адрес"},
            textmess : {required : "Введите текст сообщения"}
        }
    });*/

    $('.regwrap > form').hide();
    $('.regwrap > form:first').show();

            // regtab
    $('.regwrap > ul > li').click(function(){
        $(this).addClass('active').siblings().removeClass('active').parents('.regwrap').find('> form').hide().eq($(this).index()).show();
    });

    $('.tax > div').hide();
    $('.tax > div:first').show();

    $('.tax li').click(function(){
        /*$('.tax li').removeClass('active');
        $(this).parent().addClass('active');*/
        $(this).addClass('active').siblings().removeClass('active').parents('.tax').find('> div').hide().eq($(this).index()).show();        
    });

    $('input:checkbox').focus(function(){this.blur();});
    $('input:radio').focus(function(){this.blur();});

    /* pop up */
    $('.popenter div').click(function(){
        $('.shadow').hide();
        $('.popenter').hide();
    });
    $('.popok div').click(function(){
        $('.shadow').hide();
        $('.popok').hide();
    });
    $('.shadow').click(function(){
        $(this).hide();
        $('.popenter').hide();
        $('.popok').hide();
    });
    $('.enter').click(function(){
        $('.shadow').show();
        $('.popenter').show();
        return false;
    });

    $('.mlc .active a').click(function(){
        return false;
    });
    $('.al-list li .active').click(function(){
        return false;
    });

    $('.lc > table > tbody > tr > td').addClass(function(){
        return 'lt-' + $(this).index();
    });
    $('.lc table table td').addClass(function(){
        return 'ltt-' + $(this).index();
    });

        // show / hide table
    $('.lc table table').hide();
    $('.lc table tr:first table').show();

    $('.lc table span').click(function(){
       $(this).toggleClass('active');
       $(this).siblings().hide();
       $('.lc table span.active').siblings().show();

        if($(this).hasClass('active')) {
            $(this).text('Скрыть список товаров');
        } else {
            $(this).text('Показать список товаров');
        }

    });

    $('.lc table tr.last').css('borderBottom','0');

    $('.alphbox dl:odd').after('<div class="clr"/>');

    $('.cart tr td').addClass(function(){
        return 'ct-' + $(this).index();
    });

    //Item quantity
    $('.plus').click(function() {
        var curval = $(this).prev().val();
        var newval = parseInt(curval) + 1;
        $(this).prev().val(newval);
    });

    $('.minus').click(function() {
        var curval = $(this).prev().prev().val();
        var newval = parseInt(curval) - 1;
        if (newval >= 1) {
            $(this).prev().prev().val(newval);
        }
        else {
            return false;
        }
    });

    $('.cart tr:last').addClass('last');

    $('.prod li').hide();
    $('.prod li').slice(0,5).show();
    $('.sapr').click(function(){
        $('.prod li').show();
        $(this).hide();
    });

    $('ul.subcategory li ul').hide();
    $('ul.subcategory li em').click(function(){
        $(this).siblings().slideToggle();
    });

    $('.tpt li img').click(function(){
        var gg = $(this).attr('rel');
        $('.tpt dt img').attr('src',gg);
    });

    


    /* Sop tov */
    $('.tovwrap div').each(function(){
        $(this).next().andSelf().wrapAll('<div class="bob" />');
        $('.tovwrap div div div').unwrap();
    });

    var bob = $('.bob').length;

    for (var ug = 0; ug < bob; ug++) {
        $('.soptlist').append('<strong></strong>');
    }

    $('.soptlist strong:first').addClass('active');

    $('.bob').hide();
    $('.bob:first').show();

    $('.soptlist strong').click(function(){
        $(this).addClass('active').siblings().removeClass('active').parents('.sopt').find('.tovwrap .bob').hide().eq($(this).index()).show();
    });

    if ( $('.bob').length <= 1 ) {
        $('.soptlist').hide()
    } else {
        $('.soptlist').show()
    }


    



});