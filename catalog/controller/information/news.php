<?php
class ControllerInformationNews extends Controller {
	public function index() {
    	$this->language->load('information/news');
		
    $this->load->model('catalog/news');
		$this->load->model('tool/seo_url'); 
		
    $this->document->breadcrumbs = array();
    
        $this->document->breadcrumbs[] = array(
          'href'      => HTTP_SERVER . 'index.php?route=common/home',
          'text'      => $this->language->get('text_home'),
          'separator' => FALSE
        );
        
		if (isset($this->request->get['news_id'])) {
			$news_id = $this->request->get['news_id'];
		} else {
			$news_id = 0;
		}
		
    $news_info = $this->model_catalog_news->getNewsStory($news_id);
		
    if ($news_info) {
	  		$this->document->title = $news_info['title'];
     		
          $this->document->breadcrumbs[] = array(
        		'href'      => HTTP_SERVER . 'index.php?route=information/news',
        		'text'      => $this->language->get('heading_title'),
        		'separator' => $this->language->get('text_separator')
          );
          
          $this->document->breadcrumbs[] = array(
        		'href'      => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=information/news&news_id=' . $this->request->get['news_id']),
        		'text'      => $news_info['title'],
        		'separator' => $this->language->get('text_separator')
          );
     		
          $this->data['news_info'] = $news_info;//nahera??
     		
          $this->data['heading_title'] = $news_info['title'];
	$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($news_info['date_added']));
      $this->data['description'] = html_entity_decode($news_info['description']);
			
      $this->load->model('tool/image'); 
			
      if ($news_info['image']) {
				if ($news_info['image'] != 'no_image.jpg') {
					$image = $news_info['image'];
					if ($news_info['image_size'] == 0) {
				  		$this->data['thumb'] = $this->model_tool_image->resize($image, $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						if ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'on')) {
							$this->data['thumb'] = HTTPS_IMAGE . $image;
						} else {
							$this->data['thumb'] = HTTP_IMAGE . $image;
						}
					}
				}
			}
     		$this->data['button_news'] = $this->language->get('button_news');
			$this->data['news'] = HTTP_SERVER . 'index.php?route=information/news';
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/news.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/news.tpl';
			} else {
				$this->template = 'default/template/information/news.tpl';
			}
      
			$this->children = array(
				'common/header',
				'common/footer',
				'common/column_left',
				'common/column_right'
			);
			
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));		
	  	} else {
	  		$news_data = $this->model_catalog_news->getNews();
	  		if ($news_data) {
				foreach ($news_data as $result) {
					$short_description = explode('...', html_entity_decode($result['shot_description']));
					$this->data['news_data'][] = array(
						'title'             => $result['title'],
						'short_description' => $short_description[0],
						'href'              => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=information/news&news_id=' . $result['news_id']),
						'date_added'        => date($this->language->get('date_format_short'), strtotime($result['date_added'])) 
					);
				}
				
        $this->document->title = $this->language->get('heading_title');
				$this->document->breadcrumbs[] = array(
					'href'      => HTTP_SERVER . 'index.php?route=information/news',
					'text'      => $this->language->get('heading_title'),
					'separator' => $this->language->get('text_separator')
				);
				$this->data['heading_title'] = $this->language->get('heading_title');
				$this->data['text_read_more'] = $this->language->get('text_read_more');
				$this->data['text_date_added'] = $this->language->get('text_date_added');
				$this->data['button_continue'] = $this->language->get('button_continue');
				$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/news.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/information/news.tpl';
				} else {
					$this->template = 'default/template/information/news.tpl';
				}
				$this->children = array(
					'common/header',
					'common/footer',
					'common/column_left',
					'common/column_right'
				);
				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));		
	    	} else {
		  		$this->document->title = $this->language->get('text_error');
	     		$this->document->breadcrumbs[] = array(
	        		'href'      => HTTP_SERVER . 'index.php?route=information/news',
	        		'text'      => $this->language->get('text_error'),
	        		'separator' => $this->language->get('text_separator')
	     		);
	     		$this->data['heading_title'] = $this->language->get('text_error');
	     		$this->data['text_error'] = $this->language->get('text_error');
	     		$this->data['button_continue'] = $this->language->get('button_continue');
	     		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
				} else {
					$this->template = 'default/template/error/not_found.tpl';
				}
				$this->children = array(
					'common/header',
					'common/footer',
					'common/column_left',
					'common/column_right'
				);
				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));		
		  	}
		}
	}
}
?>
