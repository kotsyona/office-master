<?php  
class ControllerCommonHome extends Controller {
	public function index() {
		$this->language->load('common/home');
		
		$this->document->title = $this->config->get('config_title');
		$this->document->description = $this->config->get('config_meta_description');
		
		$this->data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));
		
		$this->load->model('setting/store');
		
		if (!$this->config->get('config_store_id')) {
			$this->data['welcome'] = html_entity_decode($this->config->get('config_description_' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8');
		} else {
			$store_info = $this->model_setting_store->getStore($this->config->get('config_store_id'));
			
			if ($store_info) {
				$this->data['welcome'] = html_entity_decode($store_info['description'], ENT_QUOTES, 'UTF-8');
			} else {
				$this->data['welcome'] = '';
			}
		}
						
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array();
		
		$this->load->model('checkout/extension');

		$module_data = $this->model_checkout_extension->getExtensionsByPosition('module', 'home');
		
		$this->data['modules'] = $module_data;
		
		foreach ($module_data as $result) {
			$this->children[] = 'module/' . $result['code'];
		}
		
		$this->children[] = 'common/column_right';
		$this->children[] =	'common/column_left';
		$this->children[] =	'common/footer';
		$this->children[] =	'common/header';

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('tool/seo_url');
		$this->load->helper('trim');
		###################
		$result = $this->model_catalog_product->getLatestProduct();
		if ($result) {
			$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
			if ($discount) {
				$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
				$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
			} else {
				$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
				if ($special) {
					$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
				} else {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = null;
				}
			}
			if ($result['image']) {
				$image = $result['image'];
			} else {
				$image = 'no_image.jpg';
			}
			$category = $this->model_catalog_product->getCategories($result['product_id']);
			if ($category) {
				$result['category_name'] = $category[0]['name'];
				$result['category_href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $category[0]['category_id']);
			} else {
				$result['category_name'] = null;
				$result['category_href'] = null;
			}
			$result['price'] = $price;
			$result['price_old'] = $price_old;
			$result['image'] = $this->model_tool_image->resize($image, 140, 140);
			$result['href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);

            /* WPLG */
            $product_language_data = $this->language->load('product/product');

            switch ($result['stock_status_id']) {
                /* Есть на складе */
                case '7':
                    $stock = 1;
                    $stock_text = $product_language_data['text_instock'];
                    break;
                /* Ожидаем поступление */
                case '6':
                    $stock = 0;
                    $stock_text = $product_language_data['text_arrival'];
                    break;
                /* Архивный товар */
                /* case '9':
                    $stock = 0;
                    $stock_text = $product_language_data['text_archive'];
                    break; */
                /* Архивный товар */
                default:
                    $stock = 0;
                    $stock_text = $product_language_data['text_arrival'];
                    break;
            }
            $result['stock'] = $stock;
            $result['stock_text'] = $stock_text;
            /* END WPLG */

			$this->data['latest'] = $result;
		} else {
			$this->data['latest'] = null;
		}
		#######################
		$results = $this->model_catalog_product->getLeaders();
		if ($results) {
			foreach ($results as $result) {
				$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}
				if ($result['image']) {
					$image = $result['image'];
				} else {
					$image = 'no_image.jpg';
				}
				//$category = $this->model_catalog_product->getCategories($result['product_id']);
				//$result['category_name'] = $category[0]['name'];
				//$result['category_href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&category_id=' . $category[0]['category_id']);
				$result['price'] = $price;
				$result['price_old'] = $price_old;
				$result['image'] = $this->model_tool_image->resize($image, 75, 75);
				$result['href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);

                /* WPLG */
                $product_language_data = $this->language->load('product/product');

                switch ($result['stock_status_id']) {
                    /* Есть на складе */
                    case '7':
                        $stock = 1;
                        $stock_text = $product_language_data['text_instock'];
                        break;
                    /* Ожидаем поступление */
                    case '6':
                        $stock = 0;
                        $stock_text = $product_language_data['text_arrival'];
                        break;
                    /* Архивный товар */
                    /* case '9':
                        $stock = 0;
                        $stock_text = $product_language_data['text_archive'];
                        break; */
                    /* Архивный товар */
                    default:
                        $stock = 0;
                        $stock_text = $product_language_data['text_arrival'];
                        break;
                }
                $result['stock'] = $stock;
                $result['stock_text'] = $stock_text;
                /* END WPLG */

				$this->data['leaders'][] = $result;
			}
		} else {
			$this->data['leaders'] = null;
		}
		#######################
		$results = $this->model_catalog_product->getDiscounts();
		if ($results) {
			foreach ($results as $result) {
				$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}
				if ($result['image']) {
					$image = $result['image'];
				} else {
					$image = 'no_image.jpg';
				}
				//$category = $this->model_catalog_product->getCategories($result['product_id']);
				//$result['category_name'] = $category[0]['name'];
				//$result['category_href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&category_id=' . $category[0]['category_id']);
				$result['price'] = $price;
				$result['price_old'] = $price_old;
				$result['image'] = $this->model_tool_image->resize($image, 75, 75);
				$result['href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);

                /* WPLG */
                $product_language_data = $this->language->load('product/product');

                switch ($result['stock_status_id']) {
                    /* Есть на складе */
                    case '7':
                        $stock = 1;
                        $stock_text = $product_language_data['text_instock'];
                        break;
                    /* Ожидаем поступление */
                    case '6':
                        $stock = 0;
                        $stock_text = $product_language_data['text_arrival'];
                        break;
                    /* Архивный товар */
                    /* case '9':
                        $stock = 0;
                        $stock_text = $product_language_data['text_archive'];
                        break; */
                    /* Архивный товар */
                    default:
                        $stock = 0;
                        $stock_text = $product_language_data['text_arrival'];
                        break;
                }
                $result['stock'] = $stock;
                $result['stock_text'] = $stock_text;
                /* END WPLG */

				$this->data['discounts'][] = $result;
			}
		} else {
			$this->data['discounts'] = null;
		}
		##################
		$this->load->model('catalog/category');
		$results = $this->model_catalog_category->getCategories(0);
		if ($results) {
			foreach ($results as $result) {
				$product = $this->model_catalog_product->getLatestProduct($result['category_id']);
				if ($product) {
					$discount = $this->model_catalog_product->getProductDiscount($product['product_id']);
					if ($discount) {
						$price = $this->currency->format($this->tax->calculate($discount, $product['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = $this->model_catalog_product->getProductSpecial($product['product_id']);
						if ($special) {
							$price = $this->currency->format($this->tax->calculate($special, $product['tax_class_id'], $this->config->get('config_tax')));
							$price_old = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
						} else {
							$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
							$price_old = null;
						}
					}
					$product['price'] = $price;
					$product['price_old'] = $price_old;
					$product['href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $product['product_id']);
					$result['product'] = $product;
				}
				if ($result['image']) {
					$image = $result['image'];
				} else {
					$image = 'no_image.jpg';
				}
				$result['image'] = $this->model_tool_image->resize($image, 600, 200);
				$subcategories = $this->model_catalog_category->getCategories($result['category_id']);
				for ($i = 0; $i < sizeof($subcategories); $i++) {
					$subcategories[$i]['href'] = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $result['category_id'] . '_' . $subcategories[$i]['category_id']);
				}
				$result['subcategories'] = $subcategories;
				$this->data['big_cats'][] = $result;
			}
		} else {
			$this->data['big_cats'] = null;
		}
		#######################################
		$this->load->model('catalog/news');

		$this->data['news'] = array();
		unset($result);
		$results = $this->model_catalog_news->getNewsShorts(1);
		if ($results) {
			foreach ($results as $result) {
				//$short_description = explode('.', html_entity_decode($result['description']));
				$short_description = explode('...', html_entity_decode($result['shot_description']));
				$title = html_entity_decode($result['title']);
				$this->data['news'][] = array(
					'short_description' => $short_description[0],
					'title' => $title,
					'date' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=information/news&news_id=' . $result['news_id'])
				);
			}
		} else {
			$this->data['news'] = null;
		}
		$this->data['news_all'] = HTTP_SERVER . 'index.php?route=information/news';
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}

	public function subscribe() {
		if ($this->request->server['REQUEST_METHOD'] == 'POST' and isset($this->request->post['email'])) {
			$email = $this->request->post['email'];
			if (!preg_match('/^.+@.+\..+$/', $email)) {
				return;
			}
			$q = $this->db->query('select * from '.DB_PREFIX.'newsletter where email = "'.$this->db->escape($email).'"');
			if ($q->row) {
				$this->db->query('delete from '.DB_PREFIX.'newsletter where email = "'.$this->db->escape($email).'"');
			} else {
				$this->db->query('insert into '.DB_PREFIX.'newsletter (email) values ("'.$this->db->escape($email).'")');
			}
			$this->redirect(HTTPS_SERVER . 'index.php?route=common/home');
		}
	}
}
?>