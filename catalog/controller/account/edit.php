<?php
class ControllerAccountEdit extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = HTTPS_SERVER . 'index.php?route=account/edit';

			$this->redirect(HTTPS_SERVER . 'index.php?route=account/login');
		}

		$this->language->load('account/edit');

		$this->document->title = $this->language->get('heading_title');
		
		$this->load->model('account/customer');
		$this->load->model('account/address');

		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->post['newsletter'])) {
				$this->request->post['newsletter'] = 0;
			}
			if (!isset($this->request->post['fax'])) {
				$this->request->post['fax'] = '';
			}
			$this->model_account_customer->editCustomer($this->request->post);

			$customer = $this->model_account_customer->getCustomer((int)$this->customer->getId());

			$this->model_account_address->editAddress($customer['address_id'], $this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect(HTTPS_SERVER . 'index.php?route=account/edit');
		}

      	$this->document->breadcrumbs = array();

      	$this->document->breadcrumbs[] = array(
        	'href'      => HTTP_SERVER . 'index.php?route=common/home',
        	'text'      => $this->language->get('text_home'),
        	'separator' => FALSE
      	); 

      /*	$this->document->breadcrumbs[] = array(
        	'href'      => HTTPS_SERVER . 'index.php?route=account/account',
        	'text'      => $this->language->get('text_account'),
        	'separator' => $this->language->get('text_separator')
      	);
*/
      	$this->document->breadcrumbs[] = array(
        	'href'      => HTTPS_SERVER . 'index.php?route=account/edit',
        	'text'      => $this->language->get('text_edit'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_details'] = $this->language->get('text_your_details');

		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');

		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_back'] = $this->language->get('button_back');

		$this->data['errors'] = $this->error;

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}	
		
		if (isset($this->error['telephone'])) {
			$this->data['error_telephone'] = $this->error['telephone'];
		} else {
			$this->data['error_telephone'] = '';
		}	

		$this->data['action'] = HTTPS_SERVER . 'index.php?route=account/edit';

		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
			$address_info = $this->model_account_address->getAddress($customer_info['address_id']);
		}

		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (isset($customer_info)) {
			$this->data['firstname'] = $customer_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['manager'])) {
    		$this->data['manager'] = $this->request->post['manager'];
		} elseif (isset($customer_info)) {
			$this->data['manager'] = $customer_info['manager'];
		} else {
			$this->data['manager'] = '';
		}

		if (isset($this->request->post['ur_okpo'])) {
    		$this->data['ur_okpo'] = $this->request->post['ur_okpo'];
		} elseif (isset($customer_info)) {
			$this->data['ur_okpo'] = $customer_info['ur_okpo'];
		} else {
			$this->data['ur_okpo'] = '';
		}

		if (isset($this->request->post['ur_svid'])) {
    		$this->data['ur_svid'] = $this->request->post['ur_svid'];
		} elseif (isset($customer_info)) {
			$this->data['ur_svid'] = $customer_info['ur_svid'];
		} else {
			$this->data['ur_svid'] = '';
		}

		if (isset($this->request->post['ur_id'])) {
    		$this->data['ur_id'] = $this->request->post['ur_id'];
		} elseif (isset($customer_info)) {
			$this->data['ur_id'] = $customer_info['ur_id'];
		} else {
			$this->data['ur_id'] = '';
		}

		if (isset($this->request->post['ur_account'])) {
    		$this->data['ur_account'] = $this->request->post['ur_account'];
		} elseif (isset($customer_info)) {
			$this->data['ur_account'] = $customer_info['ur_account'];
		} else {
			$this->data['ur_account'] = '';
		}

		if (isset($this->request->post['ur_bank'])) {
    		$this->data['ur_bank'] = $this->request->post['ur_bank'];
		} elseif (isset($customer_info)) {
			$this->data['ur_bank'] = $customer_info['ur_bank'];
		} else {
			$this->data['ur_bank'] = '';
		}

		if (isset($this->request->post['ur_mfo'])) {
    		$this->data['ur_mfo'] = $this->request->post['ur_mfo'];
		} elseif (isset($customer_info)) {
			$this->data['ur_mfo'] = $customer_info['ur_mfo'];
		} else {
			$this->data['ur_mfo'] = '';
		}

		if (isset($this->request->post['ur_number'])) {
    		$this->data['ur_number'] = $this->request->post['ur_number'];
		} elseif (isset($customer_info)) {
			$this->data['ur_number'] = $customer_info['ur_number'];
		} else {
			$this->data['ur_number'] = '';
		}
		
		if (isset($this->request->post['ur_code'])) {
    		$this->data['ur_code'] = $this->request->post['ur_code'];
		} elseif (isset($customer_info)) {
			$this->data['ur_code'] = $customer_info['ur_code'];
		} else {
			$this->data['ur_code'] = '';
		}
		
		if (isset($this->request->post['ur_address'])) {
    		$this->data['ur_address'] = $this->request->post['ur_address'];
		} elseif (isset($customer_info)) {
			$this->data['ur_address'] = $customer_info['ur_address'];
		} else {
			$this->data['ur_address'] = '';
		}
		
		if (isset($this->request->post['ur_company'])) {
    		$this->data['ur_company'] = $this->request->post['ur_company'];
		} elseif (isset($customer_info)) {
			$this->data['ur_company'] = $customer_info['ur_company'];
		} else {
			$this->data['ur_company'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (isset($customer_info)) {
			$this->data['lastname'] = $customer_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['newsletter'])) {
			$this->data['newsletter'] = $this->request->post['newsletter'];
		} elseif (isset($customer_info)) {
			$this->data['newsletter'] = $customer_info['newsletter'];
		} else {
			$this->data['newsletter'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (isset($customer_info)) {
			$this->data['email'] = $customer_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$this->data['telephone'] = $this->request->post['telephone'];
		} elseif (isset($customer_info)) {
			$this->data['telephone'] = $customer_info['telephone'];
		} else {
			$this->data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (isset($customer_info)) {
			$this->data['fax'] = $customer_info['fax'];
		} else {
			$this->data['fax'] = '';
		}

		if (isset($this->request->post['address_1'])) {
      		$this->data['address_1'] = $this->request->post['address_1'];
    	} elseif (isset($address_info)) {
			$this->data['address_1'] = $address_info['address_1'];
		} else {
      		$this->data['address_1'] = '';
    	}

		if (isset($this->request->post['city'])) {
      		$this->data['city'] = $this->request->post['city'];
    	} elseif (isset($address_info)) {
			$this->data['city'] = $address_info['city'];
		} else {
      		$this->data['city'] = '';
    	}

		if (isset($this->request->post['country_id'])) {
      		$this->data['country_id'] = $this->request->post['country_id'];
    	}  elseif (isset($address_info)) {
      		$this->data['country_id'] = $address_info['country_id'];
    	} else {
      		$this->data['country_id'] = $this->config->get('config_country_id');
    	}


		$this->data['back'] = HTTPS_SERVER . 'index.php?route=account/account';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/edit.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/edit.tpl';
		} else {
			$this->template = 'default/template/account/edit.tpl';
		}
		
		$this->children = array(
			'common/column_right',
			'common/footer',
			'common/column_left',
			'common/header'
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));		
	}

	private function validate() {
		if ((strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((strlen(utf8_decode($this->request->post['email'])) > 96) || (!preg_match(EMAIL_PATTERN, $this->request->post['email']))) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		$this->load->language('account/address');

		if ((strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128)) {
      		$this->error['address_1'] = $this->language->get('error_address_1');
    	}

    	if ((strlen(utf8_decode($this->request->post['city'])) < 3) || (strlen(utf8_decode($this->request->post['city'])) > 128)) {
      		$this->error['city'] = $this->language->get('error_city');
    	}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>