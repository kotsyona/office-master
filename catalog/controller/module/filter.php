<?php
class ControllerModuleFilter extends Controller {
	protected function index() {
    $this->language->load('module/filter');
    $this->data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('catalog/filter');

    $this->data['category_options'] = array();

    if (isset($this->request->get['path'])) {
			$parts = explode('_', $this->request->get['path']);
		
		  $results = $this->model_catalog_filter->getOptionByCategoriesId($parts);
			if ($results) {
			  foreach($results as $option) {
          $this->data['category_options'][] = array(
            'option_id' => $option['option_id'],
            'name' => $option['name'],
            'type' => $option['type'],
            'category_option_values' => $this->model_catalog_filter->getOptionValues($option['option_id'])
          );
        }
      }
			$path = $this->request->get['path'];
		} else {
      $path = 0;
    }
    
    $this->data['path'] = $path;
    
		$this->id = 'filter';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filter.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/filter.tpl';
		} else {
			$this->template = 'default/template/module/filter.tpl';
		}

		$this->render();
	}
}
?>