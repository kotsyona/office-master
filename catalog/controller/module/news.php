<?php
class ControllerModuleNews extends Controller {
	protected function index() {
		$this->load->language('module/news');
		
    $this->data['heading_title'] = $this->language->get('heading_title');
    $this->data['text_read_more'] = $this->language->get('text_read_more');
    $this->load->model('catalog/news');
		$this->load->model('tool/seo_url');
		
    $this->data['news'] = array();
		
    $results = $this->model_catalog_news->getNewsShorts($this->config->get('news_limit'));
		
		foreach ($results as $result) {
			//$short_description = explode('.', html_entity_decode($result['description'])); 
			$short_description = explode('...', html_entity_decode($result['shot_description'])); 
			$title = html_entity_decode($result['title']);
			$this->data['news'][] = array(
				'short_description' => $short_description[0],
				'title' => $title,
				'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=information/news&news_id=' . $result['news_id'])
			);
		}
		if ($this->config->get('news_position') == 'home') {
			$this->data['homepage'] = 'TRUE';
		}
    $this->data['news_all'] = HTTP_SERVER . 'index.php?route=information/news';
    
		$this->id       = 'news';
    
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/news.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/news.tpl';
		} else {
			$this->template = 'default/template/module/news.tpl';
		}
		$this->render();
	}
}
?>