<?php 
class ControllerProductGetAjaxProducts extends Controller {  
	public function index() { 
		$this->language->load('product/category');
		$this->load->model('catalog/category');
		$this->load->model('tool/seo_url');  
		
		if (isset($this->request->get['path'])) {
			$path = '';
		
			$parts = explode('_', $this->request->get['path']);
		
			foreach ($parts as $path_id) {
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
					if (!$path) {
						$path = $path_id;
					} else {
						$path .= '_' . $path_id;
					}

	       			$this->document->breadcrumbs[] = array(
   	    				'href'      => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $path),
    	   				'text'      => $category_info['name'],
        				'separator' => $this->language->get('text_separator')
        			);
				}
			}		
		
			$category_id = array_pop($parts);
		} else {
			$category_id = 0;
		}
		
		$category_info = $this->model_catalog_category->getCategory($category_id);
	
		if ($category_info) {
			
			$this->data['heading_title'] = $category_info['name'];
			$this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['text_sort'] = $this->language->get('text_sort');
			$this->load->model('tool/image'); 
			
			if ($category_info['image']) {
				$image = $category_info['image'];
			} else {
				$image = '';
			}

			$this->data['thumb'] = $this->model_tool_image->resize($image, $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else { 
				$page = 1;
			}	
			
			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'p.sort_order';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}
			
			if (isset($this->request->get['value_id'])) {
				$values = $this->request->get['value_id'];
			} else {
				$values = array();
			}

$this->load->helper('trim');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['value_id'])) {
				$url .= '&value_id=' . $this->request->get['value_id'];
			}
			
			$this->load->model('catalog/product');  
						 
			$category_total = $this->model_catalog_category->getTotalCategoriesByCategoryId($category_id);
			$product_total = $this->model_catalog_product->getTotalProductsByCategoryId($category_id, $values);
			
			if ($category_total || $product_total) {
        		$this->data['categories'] = array();
        		
				$results = $this->model_catalog_category->getCategories($category_id);
				
        foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}
					
					$this->data['categories'][] = array(
            			'name'  => $result['name'],
            			'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url),
            			'thumb' => $this->model_tool_image->resize($image, $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'))
          			);
        }
		
				$this->load->model('catalog/review');
				
				$this->data['button_add_to_cart'] = $this->language->get('button_add_to_cart');
				
				$this->data['products'] = array();
				
        		
				$results = $this->model_catalog_product->getProductsByCategoryId($category_id, $values, $sort, $order, ($page - 1) * $this->config->get('config_catalog_limit'), $this->config->get('config_catalog_limit'));
				
        foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}
					
					if ($this->config->get('config_review')) {
						$rating = $this->model_catalog_review->getAverageRating($result['product_id']);	
					} else {
						$rating = false;
					}
 					
					$special = FALSE;
					
					$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}
			
					$options = $this->model_catalog_product->getProductOptions($result['product_id']);
					
					if ($options) {
						$add = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);
					} else {
						$add = HTTPS_SERVER . 'index.php?route=checkout/cart&product_id=' . $result['product_id'];
					}
					
					$this->data['products'][] = array(
            			'name'    => $result['name'],
						      'model'   => $result['model'],
            			'rating'  => $rating,
						      'stars'   => sprintf($this->language->get('text_stars'), $rating),
						      'thumb'   => $this->model_tool_image->resize($image, 140, 140),
            			'price'   => $price,
            			'options' => $options,
						      'special' => $special,
						      'href'    => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&path=' . $this->request->get['path'] . '&product_id=' . $result['product_id']),
						      'add'	  => $add,
						'code' => $result['code'],
						'leader' => $result['leader'],
						'discount' => $result['discount'],
						'new' => $result['new'],
						'description' => $result['description'],
						'product_id' => $result['product_id'],
						'price_old' => $price_old,
          		);
        }

				if (!$this->config->get('config_customer_price')) {
					$this->data['display_price'] = TRUE;
				} elseif ($this->customer->isLogged()) {
					$this->data['display_price'] = TRUE;
				} else {
					$this->data['display_price'] = FALSE;
				}
		
				$url = '';
		
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}			
		
				$this->data['sorts'] = array();
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'p.sort_order-ASC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_ids=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=p.sort_order&order=ASC'
				);
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_asc'),
					'value' => 'pd.name-ASC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=pd.name&order=ASC'
				);
 
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_desc'),
					'value' => 'pd.name-DESC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=pd.name&order=DESC'
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_asc'),
					'value' => 'p.price-ASC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=p.price&order=ASC'
				); 

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_desc'),
					'value' => 'p.price-DESC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=p.price&order=DESC'
				); 
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=rating&order=DESC'
				); 
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=rating&order=ASC'
				);
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_asc'),
					'value' => 'p.model-ASC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=p.model&order=ASC'
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_desc'),
					'value' => 'p.model-DESC',
					'href'  => HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . '&value_id=' . (isset($this->request->get['value_id']) ? $this->request->get['value_id'] : '') . '&sort=p.model&order=DESC'
				);
				
				$url = '';
		
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}	

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				
				if (isset($this->request->get['value_id'])) {
				  $url .= '&value_id=' . $this->request->get['value_id'];
			  }
			
				$pagination = new Pagination();
				$pagination->total = $product_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_catalog_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/get_ajax_products&path=' . $this->request->get['path'] . $url . '&page={page}');
			
				$this->data['pagination'] = $pagination->render();
			
				$this->data['sort'] = $sort;
				$this->data['order'] = $order;
				$this->data['values'] = $values;
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/get_ajax_products.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/get_ajax_products.tpl';
				} else {
					$this->template = 'default/template/product/get_ajax_products.tpl';
				}	
				
				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);
		
				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));										
      		} else {
        		$this->document->title = $category_info['name'];
				
				$this->document->description = $category_info['meta_description'];
				
        		$this->data['heading_title'] = $category_info['name'];

        		$this->data['text_error'] = $this->language->get('text_empty');

        		$this->data['button_continue'] = $this->language->get('button_continue');

        		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';
		
				$this->data['categories'] = array();
				
				$this->data['products'] = array();
						
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/get_ajax_products.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/get_ajax_products.tpl';
				} else {
					$this->template = 'default/template/product/get_ajax_products.tpl';
				}	
				
				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);
		
				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		}
    } else {
					
			$this->response->setOutput('Ошибка: не известно category_info', $this->config->get('config_compression'));
		}
  	}
}
?>