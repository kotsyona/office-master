<?php 
class ControllerProductCategory extends Controller {  
	public function index() {
		$this->language->load('product/category');
	
		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
      		'href'      => HTTP_SERVER . 'index.php?route=common/home',
       		'text'      => $this->language->get('text_home'),
       		'separator' => FALSE
   		);	

		$this->load->model('catalog/category');
		$this->load->model('tool/seo_url');  
		
		if (isset($this->request->get['path'])) {
			$path = '';
		
			$parts = explode('_', $this->request->get['path']);
			$count = 0;
			foreach ($parts as $path_id) {
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
					if (!$path) {
						$path = $path_id;
					} else {
						$path .= '_' . $path_id;
					}

	       			$this->document->breadcrumbs[] = array(
   	    				'href'      => ($count != 0) ? $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $path) : null,
    	   				'text'      => $category_info['name'],
        				'separator' => $this->language->get('text_separator')
        			);
				}
				$count++;
			}		
		
			$category_id = array_pop($parts);
		} else {
			$category_id = 0;
		}
		
		$category_info = $this->model_catalog_category->getCategory($category_id);
	
		if ($category_info) {
	  		$this->document->title = $category_info['name'];
			
			$this->document->keywords = $category_info['meta_keywords'];
			
			$this->document->description = $category_info['meta_description'];
			
			$this->data['heading_title'] = $category_info['name'];
			
			$this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			
			$this->data['text_sort'] = $this->language->get('text_sort');
			
			$this->load->model('tool/image'); 
			
			if ($category_info['image']) {
				$image = $category_info['image'];
			} else {
				$image = '';
			}

			$this->data['thumb'] = $this->model_tool_image->resize($image, $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else { 
				$page = 1;
			}	
			
			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'p.sort_order';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['value_id'])) {
				$values = $this->request->get['value_id'];
			} else {
				$values = array();
			}
			
			if (isset($this->request->get['value_id'])) {
				$url .= '&value_id=' . $this->request->get['value_id'];
			}
			
			$this->load->model('catalog/product');  
						 
			$category_total = $this->model_catalog_category->getTotalCategoriesByCategoryId($category_id);
			$product_total = $this->model_catalog_product->getTotalProductsByCategoryId($category_id, $values);
			
			if (!$product_total and $category_total) {
				$results = $this->model_catalog_category->getCategories($category_id);
				foreach ($results as $cat) {
					$this->redirect($this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path'] . '_' . $cat['category_id'] . $url));
					break;
				}
			}
			
			if ($category_total || $product_total || 1) {
        		$this->data['categories'] = array();
        		
				$results = $this->model_catalog_category->getCategories($category_id);
				
        		foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}
					
					$this->data['categories'][] = array(
            			'name'  => $result['name'],
            			'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url),
            			'thumb' => $this->model_tool_image->resize($image, $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'))
          			);
        		}
		
				$this->load->model('catalog/review');
				
				$this->data['button_add_to_cart'] = $this->language->get('button_add_to_cart');
				
				$this->data['products'] = array();
        		
				$results = $this->model_catalog_product->getProductsByCategoryId($category_id, $values, $sort, $order, ($page - 1) * $this->config->get('config_catalog_limit'), $this->config->get('config_catalog_limit'));
				$this->load->helper('trim');
        		foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}
					
					if ($this->config->get('config_review')) {
						$rating = $this->model_catalog_review->getAverageRating($result['product_id']);	
					} else {
						$rating = false;
					}
 					
					$special = FALSE;
					
					$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}
			
					$options = $this->model_catalog_product->getProductOptions($result['product_id']);
					
					if ($options) {
						$add = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);
					} else {
						$add = HTTPS_SERVER . 'index.php?route=checkout/cart&product_id=' . $result['product_id'];
					}


                    /* WPLG */
                    $product_language_data = $this->language->load('product/product');

                    switch ($result['stock_status_id']) {
                        /* Есть на складе */
                        case '7':
                            $stock = 1;
                            $stock_text = $product_language_data['text_instock'];
                            break;
                        /* Ожидаем поступление */
                        case '6':
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                        /* Архивный товар */
                        /* case '9':
                            $stock = 0;
                            $stock_text = $product_language_data['text_archive'];
                            break; */
                        /* Архивный товар */
                        default:
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                    }
                    /* END WPLG */


					$this->data['products'][] = array(
            			'name'    => $result['name'],
						'model'   => $result['model'],
            			'rating'  => $rating,
						'stars'   => sprintf($this->language->get('text_stars'), $rating),
						'thumb'   => $this->model_tool_image->resize($image, 140, 140),
            			'price'   => $price,
            			'options' => $options,
						'special' => $special,
						'href'    => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&path=' . $this->request->get['path'] . '&product_id=' . $result['product_id']),
						'add'	  => $add,
						'price_old' => $price_old,
						'code' => $result['code'],
						'leader' => $result['leader'],
						'discount' => $result['discount'],
						'new' => $result['new'],
						'description' => $result['description'],
						'product_id' => $result['product_id'],
                        /* WPLG */
                        'stock' => $stock,
                        'stock_text' => $stock_text,
                        /* END WPLG */
          			);
        		}




				$this->language->load('module/filter');


					$this->load->model('catalog/filter');

				$this->data['category_options'] = array();

				if (isset($this->request->get['path'])) {
						$parts = explode('_', $this->request->get['path']);

					  $results = $this->model_catalog_filter->getOptionByCategoriesId($parts);
						if ($results) {
						  foreach($results as $option) {
					  $this->data['category_options'][] = array(
						'option_id' => $option['option_id'],
						'name' => $option['name'],
						'type' => $option['type'],
						'category_option_values' => $this->model_catalog_filter->getOptionValues($option['option_id'])
					  );
					}
				  }
						$path = $this->request->get['path'];
					} else {
				  $path = 0;
				}
				$this->data['filter_path'] = $path;

				if (!$this->config->get('config_customer_price')) {
					$this->data['display_price'] = TRUE;
				} elseif ($this->customer->isLogged()) {
					$this->data['display_price'] = TRUE;
				} else {
					$this->data['display_price'] = FALSE;
				}
		
				$url = '';
		
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}			
			
				if ($values) {
					$valuess = '&'. http_build_query(array('value_id' => $values), 'value_id');
				} else {
					$valuess = null;
				}
		
				$this->data['sorts'] = array();
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'p.sort_order-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=p.sort_order&order=ASC'.$valuess, true
				);
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_asc'),
					'value' => 'pd.name-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=pd.name&order=ASC'.$valuess
				);
 
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_desc'),
					'value' => 'pd.name-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=pd.name&order=DESC'.$valuess
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_asc'),
					'value' => 'p.price-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=p.price&order=ASC'.$valuess
				); 

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_desc'),
					'value' => 'p.price-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=p.price&order=DESC'.$valuess
				); 
				
				if ($this->config->get('config_review')) {
					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_desc'),
						'value' => 'rating-DESC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=rating&order=DESC'.$valuess
					); 
					
					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_asc'),
						'value' => 'rating-ASC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=rating&order=ASC'.$valuess
					);
				}
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_asc'),
					'value' => 'p.model-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=p.model&order=ASC'.$valuess
				);
 
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_desc'),
					'value' => 'p.model-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . '&sort=p.model&order=DESC'.$valuess
				);
				
				$url = '';
		
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];

				}	

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
			
				$pagination = new Pagination();
				$pagination->total = $product_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_catalog_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path']) . $url . '&page={page}'.$valuess;
			
				$this->data['pagination'] = $pagination->render();
				$this->data['path'] = $this->request->get['path'];
				if (isset($this->request->get['value_id'])) {
					$this->data['value_id'] = $this->request->get['value_id'];
				}
				$this->data['sort'] = $sort;
				$this->data['order'] = $order;
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}	
				
				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);
		
				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));										
      		} else {
        		$this->document->title = $category_info['name'];
				
				$this->document->description = $category_info['meta_description'];
				
        		$this->data['heading_title'] = $category_info['name'];

        		$this->data['text_error'] = $this->language->get('text_empty');

        		$this->data['button_continue'] = $this->language->get('button_continue');

        		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';
		
				$this->data['categories'] = array();
				
				$this->data['products'] = array();
						
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}	
				
				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);
		
				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		}
    	} else {
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}	
			
			if (isset($this->request->get['path'])) {	
	       		$this->document->breadcrumbs[] = array(
   	    			'href'      => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&path=' . $this->request->get['path'] . $url),
    	   			'text'      => $this->language->get('text_error'),
        			'separator' => $this->language->get('text_separator')
        		);
			}
				
			
			$this->document->title = $this->language->get('text_error');

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}	
			
			$this->children = array(
				'common/column_right',
				'common/column_left',
				'common/footer',
				'common/header'
			);
		
			$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
		}
  	}


	public function discount() {
		$this->language->load('product/category');

		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
      		'href'      => HTTP_SERVER . 'index.php?route=common/home',
       		'text'      => $this->language->get('text_home'),
       		'separator' => FALSE
   		);

		$this->load->model('catalog/category');
		$this->load->model('tool/seo_url');
		$this->load->helper('trim');
		$this->document->breadcrumbs[] = array(
						'href'      => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category/discount'),
						'text'      => 'Все скидки',
						'separator' => false
					);


	  		$this->document->title = 'Все скидки';

			$this->data['heading_title'] = 'Все скидки';

			$this->data['text_sort'] = $this->language->get('text_sort');

			$this->load->model('tool/image');

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'p.sort_order';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProductsByDiscount($values = NULL);

			if ($product_total) {

				$this->load->model('catalog/review');

				$this->data['button_add_to_cart'] = $this->language->get('button_add_to_cart');

				$this->data['products'] = array();

				$results = $this->model_catalog_product->getProductsByDiscount($values = NULL, $sort, $order, ($page - 1) * $this->config->get('config_catalog_limit'), $this->config->get('config_catalog_limit'));
				
        		foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}

					if ($this->config->get('config_review')) {
						$rating = $this->model_catalog_review->getAverageRating($result['product_id']);
					} else {
						$rating = false;
					}

					$special = FALSE;

					$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}

					$options = $this->model_catalog_product->getProductOptions($result['product_id']);

					if ($options) {
						$add = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);
					} else {
						$add = HTTPS_SERVER . 'index.php?route=checkout/cart&product_id=' . $result['product_id'];
					}

                    /* WPLG */
                    $product_language_data = $this->language->load('product/product');

                    switch ($result['stock_status_id']) {
                        /* Есть на складе */
                        case '7':
                            $stock = 1;
                            $stock_text = $product_language_data['text_instock'];
                            break;
                        /* Ожидаем поступление */
                        case '6':
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                        /* Архивный товар */
                        /* case '9':
                            $stock = 0;
                            $stock_text = $product_language_data['text_archive'];
                            break; */
                        /* Архивный товар */
                        default:
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                    }
                    /* END WPLG */

					$this->data['products'][] = array(
            			'name'    => $result['name'],
						'model'   => $result['model'],
            			'rating'  => $rating,
						'stars'   => sprintf($this->language->get('text_stars'), $rating),
						'thumb'   => $this->model_tool_image->resize($image, 140, 140),
            			'price'   => $price,
            			'options' => $options,
						'special' => $special,
						'href'    => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&discount=1&product_id=' . $result['product_id']),
						'add'	  => $add,
						'price_old' => $price_old,
						'code' => $result['code'],
						'leader' => $result['leader'],
						'discount' => $result['discount'],
						'new' => $result['new'],
						'description' => $result['description'],
						'product_id' => $result['product_id'],
                        /* WPLG */
                        'stock' => $stock,
                        'stock_text' => $stock_text,
                        /* END WPLG */
          			);
        		}


				$this->language->load('module/filter');


					$this->load->model('catalog/filter');

				$this->data['category_options'] = array();

				if (isset($this->request->get['path'])) {
						$parts = explode('_', $this->request->get['path']);

					  $results = $this->model_catalog_filter->getOptionByCategoriesId($parts);
						if ($results) {
						  foreach($results as $option) {
					  $this->data['category_options'][] = array(
						'option_id' => $option['option_id'],
						'name' => $option['name'],
						'type' => $option['type'],
						'category_option_values' => $this->model_catalog_filter->getOptionValues($option['option_id'])
					  );
					}
				  }
						$path = $this->request->get['path'];
					} else {
				  $path = 0;
				}
				$this->data['filter_path'] = $path;

				if (!$this->config->get('config_customer_price')) {
					$this->data['display_price'] = TRUE;
				} elseif ($this->customer->isLogged()) {
					$this->data['display_price'] = TRUE;
				} else {
					$this->data['display_price'] = FALSE;
				}

				$url = '';

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				$this->data['sorts'] = array();

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'p.sort_order-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.sort_order&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_asc'),
					'value' => 'pd.name-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=pd.name&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_desc'),
					'value' => 'pd.name-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=pd.name&order=DESC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_asc'),
					'value' => 'p.price-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.price&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_desc'),
					'value' => 'p.price-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.price&order=DESC')
				);

				if ($this->config->get('config_review')) {
					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_desc'),
						'value' => 'rating-DESC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/categorydiscount=1&sort=rating&order=DESC')
					);

					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_asc'),
						'value' => 'rating-ASC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=rating&order=ASC')
					);
				}

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_asc'),
					'value' => 'p.model-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.model&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_desc'),
					'value' => 'p.model-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.model&order=DESC')
				);

				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				$pagination = new Pagination();
				$pagination->total = $product_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_catalog_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1' . $url . '&page={page}');

				$this->data['pagination'] = $pagination->render();

				//$this->data['sort'] = $sort;
				//$this->data['order'] = $order;

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}

				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);

				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		} else {
        		$this->document->title = $category_info['name'];

				$this->document->description = $category_info['meta_description'];

        		$this->data['heading_title'] = $category_info['name'];

        		$this->data['text_error'] = $this->language->get('text_empty');

        		$this->data['button_continue'] = $this->language->get('button_continue');

        		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';

				$this->data['categories'] = array();

				$this->data['products'] = array();

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}

				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);

				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		}
  	}
	
	public function latest() {
		$this->language->load('product/category');

		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
      		'href'      => HTTP_SERVER . 'index.php?route=common/home',
       		'text'      => $this->language->get('text_home'),
       		'separator' => FALSE
   		);

		$this->load->model('catalog/category');
		$this->load->model('tool/seo_url');
		$this->load->helper('trim');
		$this->document->breadcrumbs[] = array(
						'href'      => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category/latest'),
						'text'      => 'Новые товары',
						'separator' => false
					);


	  		$this->document->title = 'Новые товары';

			$this->data['heading_title'] = 'Новые товары';

			$this->data['text_sort'] = $this->language->get('text_sort');

			$this->load->model('tool/image');

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'p.sort_order';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProductsByLatest($values = NULL);

			if ($product_total) {

				$this->load->model('catalog/review');

				$this->data['button_add_to_cart'] = $this->language->get('button_add_to_cart');

				$this->data['products'] = array();

				$results = $this->model_catalog_product->getProductsByLatest($values = NULL, $sort, $order, ($page - 1) * $this->config->get('config_catalog_limit'), $this->config->get('config_catalog_limit'));
				
        		foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}

					if ($this->config->get('config_review')) {
						$rating = $this->model_catalog_review->getAverageRating($result['product_id']);
					} else {
						$rating = false;
					}

					$special = FALSE;

					$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}

					$options = $this->model_catalog_product->getProductOptions($result['product_id']);

					if ($options) {
						$add = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);
					} else {
						$add = HTTPS_SERVER . 'index.php?route=checkout/cart&product_id=' . $result['product_id'];
					}

                    /* WPLG */
                    $product_language_data = $this->language->load('product/product');

                    switch ($result['stock_status_id']) {
                        /* Есть на складе */
                        case '7':
                            $stock = 1;
                            $stock_text = $product_language_data['text_instock'];
                            break;
                        /* Ожидаем поступление */
                        case '6':
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                        /* Архивный товар */
                        /* case '9':
                            $stock = 0;
                            $stock_text = $product_language_data['text_archive'];
                            break; */
                        /* Архивный товар */
                        default:
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                    }
                    /* END WPLG */

					$this->data['products'][] = array(
            			'name'    => $result['name'],
						'model'   => $result['model'],
            			'rating'  => $rating,
						'stars'   => sprintf($this->language->get('text_stars'), $rating),
						'thumb'   => $this->model_tool_image->resize($image, 140, 140),
            			'price'   => $price,
            			'options' => $options,
						'special' => $special,
						'href'    => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&discount=1&product_id=' . $result['product_id']),
						'add'	  => $add,
						'price_old' => $price_old,
						'code' => $result['code'],
						'leader' => $result['leader'],
						'discount' => $result['discount'],
						'new' => $result['new'],
						'description' => $result['description'],
						'product_id' => $result['product_id'],
                        /* WPLG */
                        'stock' => $stock,
                        'stock_text' => $stock_text,
                        /* END WPLG */
          			);
        		}


				$this->language->load('module/filter');


					$this->load->model('catalog/filter');

				$this->data['category_options'] = array();

				if (isset($this->request->get['path'])) {
						$parts = explode('_', $this->request->get['path']);

					  $results = $this->model_catalog_filter->getOptionByCategoriesId($parts);
						if ($results) {
						  foreach($results as $option) {
					  $this->data['category_options'][] = array(
						'option_id' => $option['option_id'],
						'name' => $option['name'],
						'type' => $option['type'],
						'category_option_values' => $this->model_catalog_filter->getOptionValues($option['option_id'])
					  );
					}
				  }
						$path = $this->request->get['path'];
					} else {
				  $path = 0;
				}
				$this->data['filter_path'] = $path;

				if (!$this->config->get('config_customer_price')) {
					$this->data['display_price'] = TRUE;
				} elseif ($this->customer->isLogged()) {
					$this->data['display_price'] = TRUE;
				} else {
					$this->data['display_price'] = FALSE;
				}

				$url = '';

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				$this->data['sorts'] = array();

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'p.sort_order-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.sort_order&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_asc'),
					'value' => 'pd.name-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=pd.name&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_desc'),
					'value' => 'pd.name-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=pd.name&order=DESC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_asc'),
					'value' => 'p.price-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.price&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_desc'),
					'value' => 'p.price-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.price&order=DESC')
				);

				if ($this->config->get('config_review')) {
					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_desc'),
						'value' => 'rating-DESC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/categorydiscount=1&sort=rating&order=DESC')
					);

					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_asc'),
						'value' => 'rating-ASC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=rating&order=ASC')
					);
				}

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_asc'),
					'value' => 'p.model-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.model&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_desc'),
					'value' => 'p.model-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.model&order=DESC')
				);

				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				$pagination = new Pagination();
				$pagination->total = $product_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_catalog_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category/latest' . $url . '&page={page}');

				$this->data['pagination'] = $pagination->render();

				//$this->data['sort'] = $sort;
				//$this->data['order'] = $order;

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}

				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);

				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		} else {
        		$this->document->title = $category_info['name'];

				$this->document->description = $category_info['meta_description'];

        		$this->data['heading_title'] = $category_info['name'];

        		$this->data['text_error'] = $this->language->get('text_empty');

        		$this->data['button_continue'] = $this->language->get('button_continue');

        		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';

				$this->data['categories'] = array();

				$this->data['products'] = array();

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}

				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);

				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		}
  	}
	
	
	public function leaders() {
		$this->language->load('product/category');

		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
      		'href'      => HTTP_SERVER . 'index.php?route=common/home',
       		'text'      => $this->language->get('text_home'),
       		'separator' => FALSE
   		);

		$this->load->model('catalog/category');
		$this->load->model('tool/seo_url');
		$this->load->helper('trim');
		$this->document->breadcrumbs[] = array(
						'href'      => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category/leaders'),
						'text'      => 'Лидеры',
						'separator' => false
					);


	  		$this->document->title = 'Лидеры';

			$this->data['heading_title'] = 'Лидеры';

			$this->data['text_sort'] = $this->language->get('text_sort');

			$this->load->model('tool/image');

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'p.sort_order';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProductsByLeaders($values = NULL);

			if ($product_total) {

				$this->load->model('catalog/review');

				$this->data['button_add_to_cart'] = $this->language->get('button_add_to_cart');

				$this->data['products'] = array();

				$results = $this->model_catalog_product->getProductsByLeaders($values = NULL, $sort, $order, ($page - 1) * $this->config->get('config_catalog_limit'), $this->config->get('config_catalog_limit'));
				
        		foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}

					if ($this->config->get('config_review')) {
						$rating = $this->model_catalog_review->getAverageRating($result['product_id']);
					} else {
						$rating = false;
					}

					$special = FALSE;

					$discount = $this->model_catalog_product->getProductDiscount($result['product_id']);
				if ($discount) {
					$price = $this->currency->format($this->tax->calculate($discount, $result['tax_class_id'], $this->config->get('config_tax')));
					$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
				} else {
					$special = $this->model_catalog_product->getProductSpecial($result['product_id']);
					if ($special) {
						$price = $this->currency->format($this->tax->calculate($special, $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), null, true, false);
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_old = null;
					}
				}

					$options = $this->model_catalog_product->getProductOptions($result['product_id']);

					if ($options) {
						$add = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&product_id=' . $result['product_id']);
					} else {
						$add = HTTPS_SERVER . 'index.php?route=checkout/cart&product_id=' . $result['product_id'];
					}

                    /* WPLG */
                    $product_language_data = $this->language->load('product/product');

                    switch ($result['stock_status_id']) {
                        /* Есть на складе */
                        case '7':
                            $stock = 1;
                            $stock_text = $product_language_data['text_instock'];
                            break;
                        /* Ожидаем поступление */
                        case '6':
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                        /* Архивный товар */
                        /* case '9':
                            $stock = 0;
                            $stock_text = $product_language_data['text_archive'];
                            break; */
                        /* Архивный товар */
                        default:
                            $stock = 0;
                            $stock_text = $product_language_data['text_arrival'];
                            break;
                    }
                    /* END WPLG */

					$this->data['products'][] = array(
            			'name'    => $result['name'],
						'model'   => $result['model'],
            			'rating'  => $rating,
						'stars'   => sprintf($this->language->get('text_stars'), $rating),
						'thumb'   => $this->model_tool_image->resize($image, 140, 140),
            			'price'   => $price,
            			'options' => $options,
						'special' => $special,
						'href'    => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/product&discount=1&product_id=' . $result['product_id']),
						'add'	  => $add,
						'price_old' => $price_old,
						'code' => $result['code'],
						'leader' => $result['leader'],
						'discount' => $result['discount'],
						'new' => $result['new'],
						'description' => $result['description'],
						'product_id' => $result['product_id'],
                        /* WPLG */
                        'stock' => $stock,
                        'stock_text' => $stock_text,
                        /* END WPLG */
          			);
        		}


				$this->language->load('module/filter');


					$this->load->model('catalog/filter');

				$this->data['category_options'] = array();

				if (isset($this->request->get['path'])) {
						$parts = explode('_', $this->request->get['path']);

					  $results = $this->model_catalog_filter->getOptionByCategoriesId($parts);
						if ($results) {
						  foreach($results as $option) {
					  $this->data['category_options'][] = array(
						'option_id' => $option['option_id'],
						'name' => $option['name'],
						'type' => $option['type'],
						'category_option_values' => $this->model_catalog_filter->getOptionValues($option['option_id'])
					  );
					}
				  }
						$path = $this->request->get['path'];
					} else {
				  $path = 0;
				}
				$this->data['filter_path'] = $path;

				if (!$this->config->get('config_customer_price')) {
					$this->data['display_price'] = TRUE;
				} elseif ($this->customer->isLogged()) {
					$this->data['display_price'] = TRUE;
				} else {
					$this->data['display_price'] = FALSE;
				}

				$url = '';

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				$this->data['sorts'] = array();

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'p.sort_order-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.sort_order&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_asc'),
					'value' => 'pd.name-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=pd.name&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_name_desc'),
					'value' => 'pd.name-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=pd.name&order=DESC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_asc'),
					'value' => 'p.price-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.price&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_price_desc'),
					'value' => 'p.price-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.price&order=DESC')
				);

				if ($this->config->get('config_review')) {
					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_desc'),
						'value' => 'rating-DESC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/categorydiscount=1&sort=rating&order=DESC')
					);

					$this->data['sorts'][] = array(
						'text'  => $this->language->get('text_rating_asc'),
						'value' => 'rating-ASC',
						'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=rating&order=ASC')
					);
				}

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_asc'),
					'value' => 'p.model-ASC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.model&order=ASC')
				);

				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_model_desc'),
					'value' => 'p.model-DESC',
					'href'  => $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category&discount=1&sort=p.model&order=DESC')
				);

				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				$pagination = new Pagination();
				$pagination->total = $product_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_catalog_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->model_tool_seo_url->rewrite(HTTP_SERVER . 'index.php?route=product/category/leaders/' . $url . '&page={page}');

				$this->data['pagination'] = $pagination->render();

				//$this->data['sort'] = $sort;
				//$this->data['order'] = $order;

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}

				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);

				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		} else {
        		$this->document->title = $category_info['name'];

				$this->document->description = $category_info['meta_description'];

        		$this->data['heading_title'] = $category_info['name'];

        		$this->data['text_error'] = $this->language->get('text_empty');

        		$this->data['button_continue'] = $this->language->get('button_continue');

        		$this->data['continue'] = HTTP_SERVER . 'index.php?route=common/home';

				$this->data['categories'] = array();

				$this->data['products'] = array();

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
				} else {
					$this->template = 'default/template/product/category.tpl';
				}

				$this->children = array(
					'common/column_right',
					'common/column_left',
					'common/footer',
					'common/header'
				);

				$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
      		}
  	}
}
?>